<?php $theme_path = base_path() . path_to_theme(); ?>
<div class="ttr_banner_header">
  <?php
    if( !empty($page['headerabovecolumn1'])|| !empty($page['headerabovecolumn2'])|| !empty($page['headerabovecolumn3'])||!empty($page['headerabovecolumn4'])):
    ?>
  <div class="ttr_banner_header_inner_above_widget_container">
    <div class="ttr_banner_header_inner_above0">
      <?php
        $showcolumn= !empty($page['headerabovecolumn1']);
        ?>
      <?php if($showcolumn): ?>
      <div class="cell1 col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12  col-12">
        <div class="headerabovecolumn1">
          <?php print render($page['headerabovecolumn1']); ?>
        </div>
      </div>
      <?php endif; ?>
      <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
      <?php
        $showcolumn= !empty($page['headerabovecolumn2']);
        ?>
      <?php if($showcolumn): ?>
      <div class="cell2 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
        <div class="headerabovecolumn2">
          <?php print render($page['headerabovecolumn2']); ?>
        </div>
      </div>
      <?php endif; ?>
      <div class=" visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
      <?php
        $showcolumn= !empty($page['headerabovecolumn3']);
        ?>
      <?php if($showcolumn): ?>
      <div class="cell3 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
        <div class="headerabovecolumn3">
          <?php print render($page['headerabovecolumn3']); ?>
        </div>
      </div>
      <?php endif; ?>
      <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
      <?php
        $showcolumn= !empty($page['headerabovecolumn4']);
        ?>
      <?php if($showcolumn): ?>
      <div class="cell4 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
        <div class="headerabovecolumn4">
          <?php print render($page['headerabovecolumn4']); ?>
        </div>
      </div>
      <?php endif; ?>
      <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
      <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
    </div>
  </div>
  <div style="clear: both;"></div>
  <?php endif; ?>
</div>
<header id="ttr_header">
  <div class="margin_collapsetop"></div>
  <div id="ttr_header_inner">
    <div class="ttr_header_element_alignment container">
      <div class="ttr_images_container"></div>
    </div>
    <?php if ($logo): ?>
    <div class="ttr_header_logo"></div>
    <?php endif; ?>
  </div>
</header>
<div class="ttr_banner_header">
  <?php
    if( !empty($page['headerbelowcolumn1'])|| !empty($page['headerbelowcolumn2'])|| !empty($page['headerbelowcolumn3'])||!empty($page['headerbelowcolumn4'])):
    ?>
  <div class="ttr_banner_header_inner_below_widget_container">
    <div class="ttr_banner_header_inner_below0">
      <?php
        $showcolumn= !empty($page['headerbelowcolumn1']);
        ?>
      <?php if($showcolumn): ?>
      <div class="cell1 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
        <div class="headerbelowcolumn1">
          <?php print render($page['headerbelowcolumn1']); ?>
        </div>
      </div>
      <?php endif; ?>
      <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
      <?php
        $showcolumn= !empty($page['headerbelowcolumn2']);
        ?>
      <?php if($showcolumn): ?>
      <div class="cell2 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
        <div class="headerbelowcolumn2">
          <?php print render($page['headerbelowcolumn2']); ?>
        </div>
      </div>
      <?php endif; ?>
      <div class=" visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
      <?php
        $showcolumn= !empty($page['headerbelowcolumn3']);
        ?>
      <?php if($showcolumn): ?>
      <div class="cell3 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
        <div class="headerbelowcolumn3">
          <?php print render($page['headerbelowcolumn3']); ?>
        </div>
      </div>
      <?php endif; ?>
      <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
      <?php
        $showcolumn= !empty($page['headerbelowcolumn4']);
        ?>
      <?php if($showcolumn): ?>
      <div class="cell4 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
        <div class="headerbelowcolumn4">
          <?php print render($page['headerbelowcolumn4']); ?>
        </div>
      </div>
      <?php endif; ?>
      <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
      <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
    </div>
  </div>
  <div style="clear: both;"></div>
  <?php endif; ?>
</div>
<div class="ttr_banner_menu">
  <?php
    if( !empty($page['menuabovecolumn1'])|| !empty($page['menuabovecolumn2'])|| !empty($page['menuabovecolumn3'])||!empty($page['menuabovecolumn4'])):
    ?>
  <div class="ttr_banner_menu_inner_above_widget_container">
    <div class="ttr_banner_menu_inner_above0">
      <?php
        $showcolumn= !empty($page['menuabovecolumn1']);
        ?>
      <?php if($showcolumn): ?>
      <div class="cell1 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
        <div class="menuabovecolumn1">
          <?php print render($page['menuabovecolumn1']); ?>
        </div>
      </div>
      <?php endif; ?>
      <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
      <?php
        $showcolumn= !empty($page['menuabovecolumn2']);
        ?>
      <?php if($showcolumn): ?>
      <div class="cell2 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
        <div class="menuabovecolumn2">
          <?php print render($page['menuabovecolumn2']); ?>
        </div>
      </div>
      <?php endif; ?>
      <div class=" visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
      <?php
        $showcolumn= !empty($page['menuabovecolumn3']);
        ?>
      <?php if($showcolumn): ?>
      <div class="cell3 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
        <div class="menuabovecolumn3">
          <?php print render($page['menuabovecolumn3']); ?>
        </div>
      </div>
      <?php endif; ?>
      <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
      <?php
        $showcolumn= !empty($page['menuabovecolumn4']);
        ?>
      <?php if($showcolumn): ?>
      <div class="cell4 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
        <div class="menuabovecolumn4">
          <?php print render($page['menuabovecolumn4']); ?>
        </div>
      </div>
      <?php endif; ?>
      <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
      <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
    </div>
  </div>
  <div style="clear: both;"></div>
  <?php endif; ?>
</div>
<div id="ttr_menu">
  <div class="margin_collapsetop"></div>
  <nav class="navbar-default navbar-expand-md navbar">
    <div id="ttr_menu_inner_in">
      <div class="ttr_menu_element_alignment container"></div>
      <div class="ttr_images_container">
        <div class="ttr_menushape1">
          <div class="html_content">
            <p style="margin:5px 5px 5px 0px;"><a href="#" target="_self" class="tt_link" href="#" class="tt_link" target="_self"><span style="font-family:'Impact';font-weight:900;font-size:1.75em;color:rgba(56,55,56,1);">Real</span><span style="font-family:'Impact';font-weight:900;font-size:1.75em;color:rgba(0,0,255,1);"> </span><span style="font-family:'Impact';font-weight:900;font-size:1.75em;color:rgba(231,76,61,1);">Steel</span></a></p>
          </div>
        </div>
      </div>
      <div id="navigationmenu">
        <div class="navbar-header">
          <button id="nav-expander" data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
          <span class="ttr_menu_toggle_button">
          <span class="sr-only">
          </span>
          <span class="icon-bar">
          </span>
          <span class="icon-bar">
          </span>
          <span class="icon-bar">
          </span>
          </span>
          <span class="ttr_menu_button_text">
          Menu
          </span>
          </button>
        </div>
        <div class="menu-center collapse navbar-collapse">
          <ul class="ttr_menu_items nav navbar-nav navbar-right">
            <?php if ($main_menu){
              $menu = variable_get('menu_main_links_source', 'main-menu');
              echo generate_menu("ttr_", $menu,"menu");}?>
            <?php $theme_path = base_path() . path_to_theme(); ?>
          </ul>
        </div>
      </div>
    </div>
  </nav>
</div>
<div class="ttr_banner_menu">
  <?php
    if( !empty($page['menubelowcolumn1'])|| !empty($page['menubelowcolumn2'])|| !empty($page['menubelowcolumn3'])||!empty($page['menubelowcolumn4'])):
    ?>
  <div class="ttr_banner_menu_inner_below_widget_container">
    <div class="ttr_banner_menu_inner_below0">
      <?php
        $showcolumn= !empty($page['menubelowcolumn1']);
        ?>
      <?php if($showcolumn): ?>
      <div class="cell1 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
        <div class="menubelowcolumn1">
          <?php print render($page['menubelowcolumn1']); ?>
        </div>
      </div>
      <?php endif; ?>
      <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
      <?php
        $showcolumn= !empty($page['menubelowcolumn2']);
        ?>
      <?php if($showcolumn): ?>
      <div class="cell2 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
        <div class="menubelowcolumn2">
          <?php print render($page['menubelowcolumn2']); ?>
        </div>
      </div>
      <?php endif; ?>
      <div class=" visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
      <?php
        $showcolumn= !empty($page['menubelowcolumn3']);
        ?>
      <?php if($showcolumn): ?>
      <div class="cell3 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
        <div class="menubelowcolumn3">
          <?php print render($page['menubelowcolumn3']); ?>
        </div>
      </div>
      <?php endif; ?>
      <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
      <?php
        $showcolumn= !empty($page['menubelowcolumn4']);
        ?>
      <?php if($showcolumn): ?>
      <div class="cell4 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
        <div class="menubelowcolumn4">
          <?php print render($page['menubelowcolumn4']); ?>
        </div>
      </div>
      <?php endif; ?>
      <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
      <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
    </div>
  </div>
  <div style="clear: both;"></div>
  <?php endif; ?>
</div>
<div class="ttr_banner_slideshow">
  <?php
    if( !empty($page['slideshowabovecolumn1'])|| !empty($page['slideshowabovecolumn2'])|| !empty($page['slideshowabovecolumn3'])||!empty($page['slideshowabovecolumn4'])):
    ?>
  <div class="ttr_banner_slideshow_inner_above_widget_container">
    <div class="ttr_banner_slideshow_inner_above0">
      <?php
        $showcolumn= !empty($page['slideshowabovecolumn1']);
        ?>
      <?php if($showcolumn): ?>
      <div class="cell1 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
        <div class="slideshowabovecolumn1">
          <?php print render($page['slideshowabovecolumn1']); ?>
        </div>
      </div>
      <?php endif; ?>
      <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
      <?php
        $showcolumn= !empty($page['slideshowabovecolumn2']);
        ?>
      <?php if($showcolumn): ?>
      <div class="cell2 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
        <div class="slideshowabovecolumn2">
          <?php print render($page['slideshowabovecolumn2']); ?>
        </div>
      </div>
      <?php endif; ?>
      <div class=" visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
      <?php
        $showcolumn= !empty($page['slideshowabovecolumn3']);
        ?>
      <?php if($showcolumn): ?>
      <div class="cell3 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
        <div class="slideshowabovecolumn3">
          <?php print render($page['slideshowabovecolumn3']); ?>
        </div>
      </div>
      <?php endif; ?>
      <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
      <?php
        $showcolumn= !empty($page['slideshowabovecolumn4']);
        ?>
      <?php if($showcolumn): ?>
      <div class="cell4 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
        <div class="slideshowabovecolumn4">
          <?php print render($page['slideshowabovecolumn4']); ?>
        </div>
      </div>
      <?php endif; ?>
      <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
      <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
    </div>
  </div>
  <div style="clear: both;"></div>
  <?php endif; ?>
</div>
<div class="ttr_slideshow">
  <div class="margin_collapsetop"></div>
  <div id="ttr_slideshow_inner">
    <ul>
      <li id="Slide0" class="ttr_slide" data-slideEffect="SlideRight">
        <a href="#"></a>
        <div class="ttr_slideshow_last">
          <div class="ttr_slideshow_element_alignment container" data-begintime="0" data-effect="SlideLeft" data-easing="linear" data-slide="None" data-duration="0"></div>
          <div class="ttr_slideshowshape01" data-effect="None" data-begintime="0" data-duration="1" data-easing="linear" data-slide="None">
            <div class="html_content">
              <p style="margin:5px 5px 5px 0px;text-align:Center;line-height: normal;"><span style="font-family:'Open Sans','Arial';font-weight:700;font-size:4.5em;color:rgba(255,255,255,1);">A Large </span><span style="font-family:'Open Sans','Arial';font-weight:700;font-size:4.5em;color:rgba(231,76,61,1);">ASSORTMENT</span><span style="font-family:'Open Sans','Arial';font-weight:700;font-size:4.5em;color:rgba(255,255,255,1);"> of Steel Products</span></p>
            </div>
          </div>
        </div>
      </li>
      <li id="Slide1" class="ttr_slide" data-slideEffect="Wipe">
        <a href="#"></a>
        <div class="ttr_slideshow_last">
          <div class="ttr_slideshow_element_alignment container" data-begintime="0" data-effect="SlideLeft" data-easing="linear" data-slide="None" data-duration="0"></div>
          <div class="ttr_slideshowshape11" data-effect="None" data-begintime="0" data-duration="1" data-easing="linear" data-slide="None">
            <div class="html_content">
              <p style="text-align:Center;line-height: normal;"><span style="font-family:'Open Sans','Arial';font-weight:700;font-size:4.5em;color:rgba(255,255,255,1);">Maecenas tristiqu </span><span style="font-family:'Open Sans','Arial';font-weight:700;font-size:4.5em;color:rgba(231,76,61,1);">ULTRICIES</span><span style="font-family:'Open Sans','Arial';font-weight:700;font-size:4.5em;color:rgba(255,255,255,1);"> pharetra magna.</span></p>
            </div>
          </div>
        </div>
      </li>
      <li id="Slide2" class="ttr_slide" data-slideEffect="SlideLeft">
        <a href="#"></a>
        <div class="ttr_slideshow_last">
          <div class="ttr_slideshow_element_alignment container" data-begintime="0" data-effect="SlideLeft" data-easing="linear" data-slide="None" data-duration="0"></div>
          <div class="ttr_slideshowshape21" data-effect="None" data-begintime="0" data-duration="1" data-easing="linear" data-slide="None">
            <div class="html_content">
              <p style="text-align:Center;line-height: normal;"><span style="font-family:'Open Sans','Calibri';font-weight:700;font-size:4.5em;color:rgba(255,255,255,1);">Fusce felis ipsum </span><span style="font-family:'Open Sans','Calibri';font-weight:700;font-size:4.5em;color:rgba(231,76,61,1);">FEUGIAT</span><span style="font-family:'Open Sans','Calibri';font-weight:700;font-size:4.5em;color:rgba(255,255,255,1);"> sed blandit</span></p>
            </div>
          </div>
        </div>
      </li>
    </ul>
  </div>
  <div class="ttr_slideshow_in">
    <div class="ttr_slideshow_last">
      <div id="nav"></div>
      <div class="ttr_slideshow_logo"></div>
    </div>
  </div>
</div>
<div class="ttr_banner_slideshow">
  <?php
    if( !empty($page['slideshowbelowcolumn1'])|| !empty($page['slideshowbelowcolumn2'])|| !empty($page['slideshowbelowcolumn3'])||!empty($page['slideshowbelowcolumn4'])):
    ?>
  <div class="ttr_banner_slideshow_inner_below_widget_container">
    <div class="ttr_banner_slideshow_inner_below0">
      <?php
        $showcolumn= !empty($page['slideshowbelowcolumn1']);
        ?>
      <?php if($showcolumn): ?>
      <div class="cell1 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
        <div class="slideshowbelowcolumn1">
          <?php print render($page['slideshowbelowcolumn1']); ?>
        </div>
      </div>
      <?php endif; ?>
      <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
      <?php
        $showcolumn= !empty($page['slideshowbelowcolumn2']);
        ?>
      <?php if($showcolumn): ?>
      <div class="cell2 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
        <div class="slideshowbelowcolumn2">
          <?php print render($page['slideshowbelowcolumn2']); ?>
        </div>
      </div>
      <?php endif; ?>
      <div class=" visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
      <?php
        $showcolumn= !empty($page['slideshowbelowcolumn3']);
        ?>
      <?php if($showcolumn): ?>
      <div class="cell3 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
        <div class="slideshowbelowcolumn3">
          <?php print render($page['slideshowbelowcolumn3']); ?>
        </div>
      </div>
      <?php endif; ?>
      <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
      <?php
        $showcolumn= !empty($page['slideshowbelowcolumn4']);
        ?>
      <?php if($showcolumn): ?>
      <div class="cell4 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
        <div class="slideshowbelowcolumn4">
          <?php print render($page['slideshowbelowcolumn4']); ?>
        </div>
      </div>
      <?php endif; ?>
      <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
      <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
    </div>
  </div>
  <div style="clear: both;"></div>
  <?php endif; ?>
</div>
<div class="margin_collapsetop"></div>
<div id="ttr_page" class="container">
  <div id="ttr_content_and_sidebar_container">
    <div id="ttr_content">
      <div id="ttr_content_margin" class="container-fluid">
        <div class="margin_collapsetop"></div>
        <?php if ($breadcrumb): ?>
        <?php print $breadcrumb; ?>
        <?php endif; ?>
        <?php
          if( !empty($page['contenttopcolumn1'])|| !empty($page['contenttopcolumn2'])|| !empty($page['contenttopcolumn3'])||!empty($page['contenttopcolumn4'])):
          ?>
        <div class="ttr_topcolumn_widget_container">
          <div class="contenttopcolumn0">
            <?php
              $showcolumn= !empty($page['contenttopcolumn1']);
              ?>
            <?php if($showcolumn): ?>
            <div class="cell1 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
              <div class="topcolumn1">
                <?php print render($page['contenttopcolumn1']); ?>
              </div>
            </div>
            <?php else: ?>
            <div class="cell1 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12"  style="background-color:transparent;">
              &nbsp;
            </div>
            <?php endif; ?>
            <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
            <?php
              $showcolumn= !empty($page['contenttopcolumn2']);
              ?>
            <?php if($showcolumn): ?>
            <div class="cell2 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
              <div class="topcolumn2">
                <?php print render($page['contenttopcolumn2']); ?>
              </div>
            </div>
            <?php else: ?>
            <div class="cell2 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12"  style="background-color:transparent;">
              &nbsp;
            </div>
            <?php endif; ?>
            <div class=" visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
            <?php
              $showcolumn= !empty($page['contenttopcolumn3']);
              ?>
            <?php if($showcolumn): ?>
            <div class="cell3 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
              <div class="topcolumn3">
                <?php print render($page['contenttopcolumn3']); ?>
              </div>
            </div>
            <?php else: ?>
            <div class="cell3 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12"  style="background-color:transparent;">
              &nbsp;
            </div>
            <?php endif; ?>
            <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
            <?php
              $showcolumn= !empty($page['contenttopcolumn4']);
              ?>
            <?php if($showcolumn): ?>
            <div class="cell4 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
              <div class="topcolumn4">
                <?php print render($page['contenttopcolumn4']); ?>
              </div>
            </div>
            <?php else: ?>
            <div class="cell4 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12"  style="background-color:transparent;">
              &nbsp;
            </div>
            <?php endif; ?>
            <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
            <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
          </div>
        </div>
        <div style="clear: both;"></div>
        <?php endif; ?>
        <div style="clear:both;"></div>
        <?php if ($tabs): ?>
        <div style="clear:both;" class="tabs"><?php print render($tabs); ?></div>
        <?php endif; ?>
        <?php print render($page['help']); ?>
        <?php print $messages; ?>
        <?php if ($action_links): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
        <?php endif; ?>
        <div class="row">
          <?php print render($page['content']); ?>
        </div>
        <?php
          if( !empty($page['contentbottomcolumn1'])|| !empty($page['contentbottomcolumn2'])|| !empty($page['contentbottomcolumn3'])||!empty($page['contentbottomcolumn4'])):
          ?>
        <div class="ttr_bottomcolumn_widget_container">
          <div class="contentbottomcolumn0">
            <?php
              $showcolumn= !empty($page['contentbottomcolumn1']);
              ?>
            <?php if($showcolumn): ?>
            <div class="cell1 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
              <div class="bottomcolumn1">
                <?php print render($page['contentbottomcolumn1']); ?>
              </div>
            </div>
            <?php else: ?>
            <div class="cell1 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12"  style="background-color:transparent;">
              &nbsp;
            </div>
            <?php endif; ?>
            <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
            <?php
              $showcolumn= !empty($page['contentbottomcolumn2']);
              ?>
            <?php if($showcolumn): ?>
            <div class="cell2 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
              <div class="bottomcolumn2">
                <?php print render($page['contentbottomcolumn2']); ?>
              </div>
            </div>
            <?php else: ?>
            <div class="cell2 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12"  style="background-color:transparent;">
              &nbsp;
            </div>
            <?php endif; ?>
            <div class=" visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
            <?php
              $showcolumn= !empty($page['contentbottomcolumn3']);
              ?>
            <?php if($showcolumn): ?>
            <div class="cell3 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
              <div class="bottomcolumn3">
                <?php print render($page['contentbottomcolumn3']); ?>
              </div>
            </div>
            <?php else: ?>
            <div class="cell3 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12"  style="background-color:transparent;">
              &nbsp;
            </div>
            <?php endif; ?>
            <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
            <?php
              $showcolumn= !empty($page['contentbottomcolumn4']);
              ?>
            <?php if($showcolumn): ?>
            <div class="cell4 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
              <div class="bottomcolumn4">
                <?php print render($page['contentbottomcolumn4']); ?>
              </div>
            </div>
            <?php else: ?>
            <div class="cell4 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12"  style="background-color:transparent;">
              &nbsp;
            </div>
            <?php endif; ?>
            <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
            <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
          </div>
        </div>
        <div style="clear: both;"></div>
        <?php endif; ?>
        <div class="margin_collapsetop"></div>
      </div>
    </div>
    <div style="clear:both;"></div>
  </div>
 <!-- start demo -->
 <div class="ttr_banner_slideshow"></div>
 <div class="margin_collapsetop"></div>
 <div id="ttr_page" class="container">
 <div id="ttr_content_and_sidebar_container">
	<div id="ttr_content">
	   <div id="ttr_html_content_margin" class="container-fluid">
		  <div class="margin_collapsetop"></div>
		  <div class="ttr_index_html_row0 row" >
			 <div class="post_column col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column00">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p style="margin:5px 5px 5px 0px;text-align:Center;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-size:2.5em;color:rgba(231,76,61,1);">Start With Us</span></p>
					  <p style="margin:10px 5px 0px 0px;text-align:Center;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-size:1.25em;color:rgba(78,78,78,1);">Fusce felis ipsum feugiat sed blandit scelerisque non sapien.</span></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
		  </div>
		  <div class="ttr_index_html_row1 row" >
			 <div class="post_column col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column10">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p><span class="ttr_image" style="float:none;display:block;overflow:hidden;margin:0px 0px 0px 0px;"><span><img class="ttr_uniform" style="max-height:667px;max-width:1000px;" src="/sites/all/themes/realsteelbeagles/images/107.jpg" /></span></span></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
			 <div class="post_column col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column11">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p style="margin: 20px;line-height:1.56em;"><span style="font-family:'Open Sans','Arial';font-size:1.875em;color:rgba(78,78,78,1);">P</span><span style="font-family:'Open Sans','Arial';font-size:1.875em;color:rgba(78,78,78,1);">rofessional context it often happens that private or corporate clients</span></p>
					  <p style="margin:30px 5px 5px 0px;line-height:1.56em;"><span style="font-family:'Open Sans','Arial';font-size:0.938em;color:rgba(105,105,105,1);">corder a publication to be madnd presented with the actual content not being ready. Think of a news blogat's filled with content hourly on tng live. However, reviewers ten be distracted by comprehaper or the internet. he are likel layout and its elements. Besides, rando uneptable risk inonments. </span><a href="#" target="_self" class="tt_link" href="#" class="tt_link" target="_self"><span style="font-family:'Open Sans','Arial';font-weight:700;font-size:0.938em;color:rgba(231,76,61,1);">Lorem ipsum</span><span style="font-family:'Open Sans','Arial';font-weight:700;font-size:0.938em;"> </span></a><span style="font-family:'Open Sans','Arial';font-size:0.938em;color:rgba(105,105,105,1);">and quite likely since the sixteenth century.</span></p>
					  <p style="margin:30px 5px 5px 0px;line-height:1.56em;"><span><br style="font-family:'Open Sans','Arial';" /><a href="#" target="_self" class="btn btn-sm btn-default">Learn More</a></span></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
		  </div>
		  <div class="ttr_index_html_row2 row" >
			 <div class="post_column col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column20">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p style="margin:5px 5px 5px 0px;text-align:Center;"><span style="font-family:'Open Sans','Arial';font-size:1.875em;color:rgba(255,255,255,1);">Our Features</span></p>
					  <p><br style="font-family:'Open Sans','Arial';" /></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
			 <div class="post_column col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column21">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p style="margin:0px 5px 0px 0px;text-align:Center;line-height:1.88em;"><br style="font-family:'Open Sans','Arial';" /><span class="ttr_image" style="float:none;display:block;overflow:hidden;margin:0px 0px 0px 0px;"><span><img style="max-height:90px;max-width:90px;" src="/sites/all/themes/realsteelbeagles/images/19.png" /></span></span></p>
					  <p style="margin:0px 5px 0px 0px;text-align:Center;line-height:1.88em;"><span style="font-family:'Open Sans','Arial';font-size:1.25em;color:rgba(255,255,255,1);">Our Steel Factory</span></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
			 <div class="post_column col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column22">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p style="margin:5px 5px 5px 0px;text-align:Center;line-height:1.88em;"><br style="font-family:'Open Sans','Arial';" /><span class="ttr_image" style="float:none;display:block;overflow:hidden;margin:0px 0px 0px 0px;"><span><img style="max-height:90px;max-width:90px;" src="/sites/all/themes/realsteelbeagles/images/20.png" /></span></span></p>
					  <p style="margin:5px 5px 5px 0px;text-align:Center;line-height:1.88em;"><span style="font-family:'Open Sans','Arial';font-size:1.25em;color:rgba(255,255,255,1);">Our Men Power</span></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
			 <div class="post_column col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column23">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p style="margin:5px 5px 5px 0px;text-align:Center;line-height:1.88em;"><br style="font-family:'Open Sans','Arial';" /><span class="ttr_image" style="float:none;display:block;overflow:hidden;margin:0px 0px 0px 0px;"><span><img style="max-height:90px;max-width:90px;" src="/sites/all/themes/realsteelbeagles/images/21.png" /></span></span></p>
					  <p style="margin:5px 5px 5px 0px;text-align:Center;line-height:1.88em;"><span style="font-family:'Open Sans','Arial';font-size:1.25em;color:rgba(255,255,255,1);">Our Top Marketing</span></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
			 <div class="post_column col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column24">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p style="margin:5px 5px 5px 0px;text-align:Center;line-height:1.88em;"><br style="font-family:'Open Sans','Arial';" /><span class="ttr_image" style="float:none;display:block;overflow:hidden;margin:0px 0px 0px 0px;"><span><img style="max-height:90px;max-width:90px;" src="/sites/all/themes/realsteelbeagles/images/22.png" /></span></span></p>
					  <p style="margin:5px 5px 5px 0px;text-align:Center;line-height:1.88em;"><span style="font-family:'Open Sans','Arial';font-size:1.25em;color:rgba(255,255,255,1);">Our Services</span></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
		  </div>
		  <div class="ttr_index_html_row3 row" >
			 <div class="post_column col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column30">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p style="margin:5px 5px 5px 0px;text-align:Center;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-size:1.875em;color:rgba(78,78,78,1);">Our Policy</span></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
		  </div>
		  <div class="ttr_index_html_row4 row" >
			 <div class="post_column col-xl-5 col-lg-5 col-md-12 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column40">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p style="text-align:Center;"><span class="ttr_image" style="float:Left;overflow:hidden;margin:0px 0px 0px 0px;"><span><img class="ttr_uniform" style="max-height:545px;max-width:400px;" src="/sites/all/themes/realsteelbeagles/images/23.jpg" /></span></span></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
			 <div class="post_column col-xl-7 col-lg-7 col-md-12 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column41">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p style="margin:5px 5px 5px 0px;"><span style="font-family:'Open Sans','Arial';font-size:1.562em;color:rgba(78,78,78,1);">We Are Here To Help You?</span></p>
					  <p style="margin:20px 5px 5px 0px;line-height:1.56em;"><span style="font-family:'Open Sans','Arial';font-size:1.25em;color:rgba(231,76,61,1);">Praesent vestibulum enean nonconsectetuer adipiscing elit.</span></p>
					  <p style="margin:10px 5px 5px 0px;line-height:1.56em;"><span style="font-family:'Open Sans','Arial';font-size:0.938em;color:rgba(105,105,105,1);">Praesent vestibulum molestie lacus. Aenean nonummy hendrerimauris. Phasellus porta. Fusce suscipit varius mi. Lorem ipsum dolor sit am consectetuer adipiscing elit. Praesent vestibulum molestie lacus. Morbi nuor siet, consectetuer adipiscing elit. Mauris fermentum dictum magna. Sed laoreet al leo. </span></p>
					  <p style="margin:10px 5px 5px 0px;line-height:1.56em;"><span style="font-family:'Open Sans','Arial';font-size:0.938em;color:rgba(105,105,105,1);">Ut tellus dolor, dapibus eget, elementum vel, cursus eleifend, elit. Aene odio, gravida at, cursus nec, luctus a, lorem. Maecenas tristique orci em. Duis ultricies pharetra magna onec accumsan malesuada orci.Ut tellus dolor, dapibus eget, elementum vel, cursus eleifenit. Aenean auctoi et urna.nc odio, gravida at, cursus nec, luctus a, lorem. Maecenas tristii em. Duis ultricies pharetra eget, eleum vel, cursus eleifend, elit. </span></p>
					  <p style="margin:10px 5px 5px 0px;line-height:1.56em;"><span style="font-family:'Open Sans','Arial';font-size:0.938em;color:rgba(105,105,105,1);">Aenean auctoi et urna.nc odio, gravida at, cursus nec, luctus a, Maecenas trisque orci em. Duis ultricies pharetra magna onec accumsan malllus dolor, dapiet, elementum vel, cursus eleifend, elit. Aenean auctoi et urna.nc odio, grauctus a, lorem. Mastique orci em.</span></p>
					  <p style="margin:10px 5px 5px 0px;line-height:1.56em;"><br style="font-family:'Open Sans','Arial';font-size:1em;color:rgba(105,105,105,1);" /></p>
					  <p style="margin:10px 5px 5px 0px;line-height:1.56em;"><span><br style="font-family:'Open Sans','Arial';" /><a href="#" target="_self" class="btn btn-sm btn-default">Click Here</a></span></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
		  </div>
		  <div class="ttr_index_html_row5 row" >
			 <div class="post_column col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column50">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p style="margin:20px 0px 0px 0px;text-align:Center;line-height:1.25em;"><span style="font-family:'Open Sans','Arial';font-weight:700;font-size:2.5em;color:rgba(255,255,255,1);">Nullam dignissim convallis est.Quisque aliquam. </span></p>
					  <p style="margin:20px 0px 0px 0px;text-align:Center;line-height:1.25em;"><br style="font-family:'Open Sans','Arial';font-weight:700;" /></p>
					  <p style="margin:20px 0px 0px 0px;text-align:Center;line-height:1.25em;"><span><br style="font-family:'Open Sans','Arial';font-weight:700;" /><a href="#" target="_self" class="btn btn-sm btn-default">Get Now</a></span></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
		  </div>
		  <div class="ttr_index_html_row6 row" >
			 <div class="post_column col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column60">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p style="margin:6.67px 0px 6.67px 0px;text-align:Center;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-size:1.875em;color:rgba(52,52,52,1);">Testimonial's</span></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
		  </div>
		  <div class="ttr_index_html_row7 row" >
			 <div class="post_column col-xl-3 col-lg-3 col-md-5 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column70">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p><span class="ttr_image" style="float:none;display:block;overflow:hidden;margin:0px 0px 0px 0px;"><span><img style="max-height:120px;max-width:120px;" src="/sites/all/themes/realsteelbeagles/images/113.png" /></span></span></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
			 <div class="post_column col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column71">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p style="margin:6.67px 0px 6.67px 0px;line-height:1.5em;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-size:0.938em;color:rgba(105,105,105,1);">Quo ad clita prompta, has causdolorem commune, possim habemudefinitiones. Tota corrumpit voluptatui pri id, imul.</span></p>
					  <p style="margin:20px 0px 20px 0px;font-weight:700;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-size:1.125em;color:rgba(231,76,61,1);">CEO</span><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-weight:400;font-size:1.125em;color:rgba(52,52,52,1);"> </span><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-weight:400;font-size:1.125em;color:rgba(157,157,157,1);">of Mord Fustang</span></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
			 <div class=" visible-md-block d-md-block" style="clear: both;"></div>
			 <div class="post_column col-xl-3 col-lg-3 col-md-5 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column72">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p><span class="ttr_image" style="float:none;display:block;overflow:hidden;margin:0px 0px 0px 0px;"><span><img style="max-height:120px;max-width:120px;" src="/sites/all/themes/realsteelbeagles/images/114.png" /></span></span></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
			 <div class="post_column col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column73">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p style="margin:6.67px 0px 6.67px 0px;line-height:1.5em;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-size:0.938em;color:rgba(105,105,105,1);">Quo ad clita prompta, has causdolorem commune, possim habemudefinitiones. Tota corrumpit voluptatui pri id, isimul.</span></p>
					  <p style="margin:20px 0px 20px 0px;font-weight:700;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-size:1.125em;color:rgba(231,76,61,1);">CEO</span><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-weight:400;font-size:1.125em;color:rgba(52,52,52,1);"> </span><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-weight:400;font-size:1.125em;color:rgba(157,157,157,1);">of Mord Fustang</span></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-lg-block d-xl-block d-lg-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
		  </div>
		  <div class="ttr_index_html_row8 row" >
			 <div class="post_column col-xl-3 col-lg-3 col-md-5 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column80">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p><span class="ttr_image" style="float:none;display:block;overflow:hidden;margin:0px 0px 0px 0px;"><span><img style="max-height:151px;max-width:152px;" src="/sites/all/themes/realsteelbeagles/images/115.png" /></span></span></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
			 <div class="post_column col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column81">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p style="margin:6.67px 0px 6.67px 0px;line-height:1.5em;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-size:0.938em;color:rgba(105,105,105,1);">Quo ad clita prompta, has causdolorem commune, possim habemudefinitiones. Tota corrumpit voluptatui pri id, simul.</span></p>
					  <p style="margin:20px 0px 20px 0px;font-weight:700;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-size:1.125em;color:rgba(231,76,61,1);">CEO</span><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-size:1.125em;color:rgba(52,52,52,1);"> </span><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-weight:400;font-size:1.125em;color:rgba(157,157,157,1);">of Mord Fustang</span></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
			 <div class=" visible-md-block d-md-block" style="clear: both;"></div>
			 <div class="post_column col-xl-3 col-lg-3 col-md-5 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column82">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p><span class="ttr_image" style="float:none;display:block;overflow:hidden;margin:0px 0px 0px 0px;"><span><img style="max-height:150px;max-width:150px;" src="/sites/all/themes/realsteelbeagles/images/116.png" /></span></span></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
			 <div class="post_column col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column83">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p style="margin:6.67px 0px 6.67px 0px;line-height:1.5em;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-size:0.938em;color:rgba(105,105,105,1);">Quo ad clita prompta, has causdolorem commune, possim habemudefinitiones. Tota corrumpit voluptatui pri simul.</span></p>
					  <p style="margin:20px 0px 20px 0px;font-weight:700;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-size:1.125em;color:rgba(231,76,61,1);">CEO</span><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-size:1.125em;color:rgba(52,52,52,1);"> </span><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-weight:400;font-size:1.125em;color:rgba(157,157,157,1);">of Mord Fustang</span></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-lg-block d-xl-block d-lg-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
		  </div>
		  <div class="ttr_index_html_row9 row" id="about-us" >
			 <div class="post_column col-xl-12 col-lg-12 col-md-6 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column90">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p style="margin:5px 5px 5px 0px;text-align:Center;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-size:2.5em;color:rgba(231,76,61,1);">About Us</span></p>
					  <p style="margin:10px 5px 0px 0px;text-align:Center;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-size:1.25em;color:rgba(78,78,78,1);">Fusce felis ipsum feugiat sed blandit scelerisque non sapien.</span></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-lg-block d-xl-block d-lg-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
		  </div>
		  <div class="ttr_index_html_row10 row" >
			 <div class="post_column col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column100">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p style="margin:5px 5px 5px 0px;"><span style="font-family:'Open Sans','Arial';font-size:1.562em;color:rgba(78,78,78,1);">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. </span></p>
					  <p style="margin:20px 5px 5px 0px;line-height:1.56em;"><span style="font-family:'Open Sans','Arial';font-size:0.938em;color:rgba(105,105,105,1);">Donec varius Quisque nulla. Vestibunisl, porta vel, scelerisque eget, malesuada at, neque. Vivamus eget nibh. Etiam curss leo eu dui uere, nec consectetur ante sodales. Fusce porta, odio vel consectetur moarc vulputte diam, vel rutrum odio purus eget nisi.Quisque nulla. Vestibunislus leo vel metus. Nulla facilisiest quam, eget lobortis est iaculis id. Suspendisse sit amet neqe i. Donec in quam sit amet metus consectetur ultrices. Duis venenatis leo eu dui uere, nec consectetur ante sodales. Fusce porta, odio vel consectetur moarc vulputte diam, vel rutrum odio purus eget nisi.Quisque nulla. Vestibunisl, porta vel, scerisque eget, malesuada at, neque. Vivamus eget nibh.</span></p>
					  <p style="margin:20px 5px 5px 0px;line-height:1.56em;"><span><br style="font-family:'Open Sans','Arial';" /><a href="#" target="_self" class="btn btn-sm btn-default">Read More</a></span></p>
					  <p style="margin:20px 5px 5px 0px;line-height:1.56em;"><br style="font-family:'Open Sans','Arial';" /></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
			 <div class="post_column col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column101">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p><span class="ttr_image" style="float:none;display:block;overflow:hidden;margin:0px 0px 0px 0px;"><span><img class="ttr_uniform" style="max-height:727px;max-width:1000px;" src="/sites/all/themes/realsteelbeagles/images/117.jpg" /></span></span></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
		  </div>
		  <div class="ttr_index_html_row11 row" >
			 <div class="post_column col-xl-12 col-lg-12 col-md-6 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column110">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p style="text-align:Center;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-size:1.875em;color:rgba(52,52,52,1);">Free </span><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-weight:700;font-size:1.875em;color:rgba(231,76,61,1);">Cvil Egineering </span><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-size:1.875em;color:rgba(52,52,52,1);">Training</span></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-lg-block d-xl-block d-lg-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
		  </div>
		  <div class="ttr_index_html_row12 row" >
			 <div class="post_column col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column120">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p style="margin:5px 5px 5px 0px;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-size:1.125em;color:rgba(52,52,52,1);">Pellentesque nec convallis sapien. Nulla facilisi. Nulla sed lectus ipsum. Curabitur in massa orci.</span></p>
					  <p style="margin:20px 5px 0px 0px;line-height:1.5em;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-size:0.938em;color:rgba(105,105,105,1);">Curabitur in massa orci. Cras dolor est, varius vitae mattis vel, sollicitudin quis lacus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce quis maximus mau.</span></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
			 <div class="post_column col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column121">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p style="margin:5px 5px 5px 0px;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-size:1.125em;color:rgba(52,52,52,1);">Nulla facilisi. Nulla sed lectus ipsum. Curabitur in massa orci. Cras dolor est, varius vitae mattis vel, sollicit</span></p>
					  <p style="margin:20px 5px 0px 0px;line-height:1.5em;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-size:0.938em;color:rgba(105,105,105,1);">Curabitur in massa orci. Cras dolor est, varius vitae mattis vel, sollicitudin quis lacus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce quis maximus mau.</span></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
			 <div class="post_column col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column122">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p style="margin:5px 5px 5px 0px;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-size:1.125em;color:rgba(52,52,52,1);">Lectus ipsum. Curabitur in massa orci. Cras dolor est, varius vitae mattis vel, sollicit.</span></p>
					  <p style="margin:20px 5px 0px 0px;line-height:1.5em;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-size:0.938em;color:rgba(105,105,105,1);">Curabitur in massa orci. Cras dolor est, varius vitae mattis vel, sollicitudin quis lacus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce quis maximus mau.</span></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
			 <div class="post_column col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column123">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p style="margin:5px 5px 5px 0px;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-size:1.125em;color:rgba(52,52,52,1);">Sapien ulla sed lectus ipsum. Curabitur in massa orci. Cras dolor est, varius vitae mattis.</span></p>
					  <p style="margin:20px 5px 0px 0px;line-height:1.5em;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-size:0.938em;color:rgba(105,105,105,1);">Curabitur in massa orci. Cras dolor est, varius vitae mattis vel, sollicitudin quis lacus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce quis maximus mau.</span></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
		  </div>
		  <div class="ttr_index_html_row13 row" >
			 <div class="post_column col-xl-12 col-lg-12 col-md-6 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column130">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p style="text-align:Center;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-size:1.875em;color:rgba(78,78,78,1);">Our Team</span></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-lg-block d-xl-block d-lg-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
		  </div>
		  <div class="ttr_index_html_row14 row" >
			 <div class="post_column col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column140">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p style="margin:0px 0px 0px 0px;text-align:Center;"><span class="ttr_image" style="float:none;display:block;overflow:hidden;margin:0px 0px 30px 0px;"><span><img style="max-height:300px;max-width:270px;" src="/sites/all/themes/realsteelbeagles/images/30.jpg" /></span></span><span style="font-family:'Open Sans','Arial';font-weight:700;font-size:1em;color:rgba(52,52,52,1);">Civil Engineer</span></p>
					  <p style="margin:5px 0px 5px 0px;text-align:Center;"><span style="font-family:'Open Sans','Arial';font-size:1.25em;color:rgba(78,78,78,1);">Mr Stark</span></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
			 <div class="post_column col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column141">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p style="margin:0px 0px 0px 0px;text-align:Center;"><span class="ttr_image" style="float:none;display:block;overflow:hidden;margin:0px 0px 30px 0px;"><span><img style="max-height:300px;max-width:270px;" src="/sites/all/themes/realsteelbeagles/images/31.jpg" /></span></span><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-weight:700;font-size:1em;color:rgba(52,52,52,1);">Supp. Civil Engineer</span></p>
					  <p style="margin:5px 0px 5px 0px;text-align:Center;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-size:1.25em;color:rgba(105,105,105,1);">Mr Clark</span></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
			 <div class="post_column col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column142">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p style="text-align:Center;"><span class="ttr_image" style="float:none;display:block;overflow:hidden;margin:0px 0px 30px 0px;"><span><img style="max-height:300px;max-width:270px;" src="/sites/all/themes/realsteelbeagles/images/32.jpg" /></span></span><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-weight:700;font-size:1em;color:rgba(52,52,52,1);">CEO</span></p>
					  <p style="margin:5px 0px 5px 0px;text-align:Center;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-size:1.25em;color:rgba(105,105,105,1);">Mr James</span></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
			 <div class="post_column col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column143">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p style="text-align:Center;"><span class="ttr_image" style="float:none;display:block;overflow:hidden;margin:0px 0px 30px 0px;"><span><img style="max-height:300px;max-width:270px;" src="/sites/all/themes/realsteelbeagles/images/33.jpg" /></span></span><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-weight:700;font-size:1em;color:rgba(52,52,52,1);">Sr. Civil Engineer</span></p>
					  <p style="margin:5px 0px 5px 0px;text-align:Center;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-size:1.25em;color:rgba(105,105,105,1);">Mr Devin</span></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
		  </div>
		  <div class="ttr_index_html_row15 row" id="service" >
			 <div class="post_column col-xl-12 col-lg-12 col-md-6 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column150">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p style="margin:5px 5px 5px 0px;text-align:Center;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-size:2.5em;color:rgba(231,76,61,1);">Services</span></p>
					  <p style="margin:10px 5px 0px 0px;text-align:Center;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-size:1.25em;color:rgba(78,78,78,1);">Fusce felis ipsum feugiat sed blandit scelerisque non sapien.</span></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-lg-block d-xl-block d-lg-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
		  </div>
		  <div class="ttr_index_html_row16 row" >
			 <div class="post_column col-xl-1 col-lg-1 col-md-6 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column160">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p style="text-align:Center;"><span class="ttr_image" style="float:none;display:block;overflow:hidden;margin:0px 0px 0px 0px;"><span><img style="max-height:52px;max-width:52px;" src="/sites/all/themes/realsteelbeagles/images/34.png" /></span></span></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
			 <div class="post_column col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column161">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p style="margin:2px 0px 0px 0px;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-weight:700;font-size:1.25em;color:rgba(255,255,255,1);">Service1</span></p>
					  <p style="margin:20px 0px 0px 0px;line-height:1.5em;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-size:0.938em;color:rgba(255,255,255,1);">Lorem ipsum dolor sit amet, test link adipiscing elit.Nullam dignissim convallis est.Quisque aliquam. </span></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
			 <div class="post_column col-xl-1 col-lg-1 col-md-6 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column162">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p style="text-align:Center;"><span class="ttr_image" style="float:none;display:block;overflow:hidden;margin:0px 0px 0px 0px;"><span><img style="max-height:52px;max-width:52px;" src="/sites/all/themes/realsteelbeagles/images/35.png" /></span></span></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
			 <div class="post_column col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column163">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p style="margin:2px 0px 0px 0px;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-weight:700;font-size:1.25em;color:rgba(255,255,255,1);">Service2</span></p>
					  <p style="margin:20px 0px 0px 0px;line-height:1.5em;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-size:0.938em;color:rgba(255,255,255,1);">Lorem ipsum dolor sit amet, test link adipiscing elit.Nullam dignissim convallis est.Quisque aliquam. </span></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
			 <div class="post_column col-xl-1 col-lg-1 col-md-6 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column164">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p style="margin:5px 5px 5px 0px;text-align:Center;"><span class="ttr_image" style="float:none;display:block;overflow:hidden;margin:0px 0px 0px 0px;"><span><img style="max-height:50px;max-width:50px;" src="/sites/all/themes/realsteelbeagles/images/36.png" /></span></span></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
			 <div class="post_column col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column165">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p style="margin:2px 0px 0px 0px;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-weight:700;font-size:1.25em;color:rgba(255,255,255,1);">Service3</span></p>
					  <p style="margin:20px 0px 0px 0px;line-height:1.5em;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-size:0.938em;color:rgba(255,255,255,1);">Lorem ipsum dolor sit amet, test link adipiscing elit.Nullam dignissim convallis est.Quisque aliquam. </span></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
			 <div class="post_column col-xl-1 col-lg-1 col-md-6 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column166">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p style="text-align:Center;"><span class="ttr_image" style="float:none;display:block;overflow:hidden;margin:0px 0px 0px 0px;"><span><img style="max-height:52px;max-width:52px;" src="/sites/all/themes/realsteelbeagles/images/37.png" /></span></span></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
			 <div class="post_column col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column167">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p style="margin:2px 0px 0px 0px;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-weight:700;font-size:1.25em;color:rgba(255,255,255,1);">Service4</span></p>
					  <p style="margin:20px 0px 0px 0px;line-height:1.5em;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-size:0.938em;color:rgba(255,255,255,1);">Lorem ipsum dolor sit amet, test link adipiscing elit.Nullam dignissim convallis est.Quisque aliquam. </span></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
			 <div class="post_column col-xl-1 col-lg-1 col-md-6 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column168">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p style="text-align:Center;"><span class="ttr_image" style="float:none;display:block;overflow:hidden;margin:0px 0px 0px 0px;"><span><img style="max-height:52px;max-width:52px;" src="/sites/all/themes/realsteelbeagles/images/38.png" /></span></span></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
			 <div class="post_column col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column169">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p style="margin:2px 0px 0px 0px;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-weight:700;font-size:1.25em;color:rgba(255,255,255,1);">Service5</span></p>
					  <p style="margin:20px 0px 0px 0px;line-height:1.5em;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-size:0.938em;color:rgba(255,255,255,1);">Lorem ipsum dolor sit amet, test link adipiscing elit.Nullam dignissim convallis est.Quisque aliquam. </span></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
			 <div class="post_column col-xl-1 col-lg-1 col-md-6 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column1610">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p style="text-align:Center;"><span class="ttr_image" style="float:none;display:block;overflow:hidden;margin:0px 0px 0px 0px;"><span><img style="max-height:52px;max-width:52px;" src="/sites/all/themes/realsteelbeagles/images/39.png" /></span></span></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
			 <div class="post_column col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column1611">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p style="margin:2px 0px 0px 0px;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-weight:700;font-size:1.25em;color:rgba(255,255,255,1);">Service6</span></p>
					  <p style="margin:20px 0px 0px 0px;line-height:1.5em;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-size:0.938em;color:rgba(255,255,255,1);">Lorem ipsum dolor sit amet, test link adipiscing elit.Nullam dignissim convallis est.Quisque aliquam. </span></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
		  </div>
		  <div class="ttr_index_html_row17 row" >
			 <div class="post_column col-xl-12 col-lg-12 col-md-6 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column170">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p style="margin:5px 5px 5px 0px;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-size:1.875em;color:rgba(78,78,78,1);">Clients Choose Us!</span></p>
					  <p style="margin:5px 5px 5px 0px;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-size:1em;color:rgba(105,105,105,1);">Nullam dignissim convallis est.Quisque aliquam. Donec faucibus. </span></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-lg-block d-xl-block d-lg-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
		  </div>
		  <div class="ttr_index_html_row18 row" >
			 <div class="post_column col-xl-12 col-lg-12 col-md-6 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column180">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p><span class="ttr_image" style="float:none;display:block;overflow:hidden;margin:0px 0px 0px 0px;"><span><img class="ttr_uniform" style="max-height:493px;max-width:1155px;" src="/sites/all/themes/realsteelbeagles/images/128.jpg" /></span></span></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-lg-block d-xl-block d-lg-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
		  </div>
		  <div class="ttr_index_html_row19 row" >
			 <div class="post_column col-xl-12 col-lg-12 col-md-6 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column190">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p style="margin:5px 5px 5px 0px;text-align:Center;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-size:1.875em;color:rgba(78,78,78,1);">We Are The Best</span></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-lg-block d-xl-block d-lg-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
		  </div>
		  <div class="ttr_index_html_row20 row" >
			 <div class="post_column col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column200">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p style="margin:5px 5px 5px 0px;line-height:1.56em;"><span style="font-family:'Open Sans','Arial';font-size:1.875em;color:rgba(78,78,78,1);">Fusce suscipit magnis di parturient montur ridiculus mus.</span></p>
					  <p style="margin:20px 5px 5px 0px;line-height:1.56em;"><span style="font-family:'Open Sans','Arial';font-size:0.938em;color:rgba(105,105,105,1);">Cum sociis natoque penatibus et magnis dis parturient montes, nasctur ridiculus mus. Praesent vestibulum molestie lacus. Aenean nonummy hit mauris. Phasellus porta. Praesent vestibulumCum sociis natoque penatibus t magnis dis parturient montes, nasctur ridiculus mus. Praesent vestibulum tie lacus. Aenean nonummy hit mauris. Phasellus porta. Cum sociis natoque patibus et magnis dis parturient montes, nasctur ridiculus mus. Praes vestibulumolestie lacus. Aenean nonummy hit mauris. Phasellus porta. </span></p>
					  <p style="margin:20px 5px 5px 0px;line-height:1.56em;"><span><br style="font-family:'Open Sans','Arial';" /><a href="#" target="_self" class="btn btn-sm btn-default">Read More</a></span></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
			 <div class="post_column col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column201">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p><span class="ttr_image" style="float:none;display:block;overflow:hidden;margin:0px 0px 0px 0px;"><span><img class="ttr_uniform" style="max-height:667px;max-width:1000px;" src="/sites/all/themes/realsteelbeagles/images/129.jpg" /></span></span></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
		  </div>
		  <div class="ttr_index_html_row21 row" id="projects" >
			 <div class="post_column col-xl-12 col-lg-12 col-md-6 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column210">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p style="margin:5px 5px 5px 0px;text-align:Center;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-size:2.5em;color:rgba(231,76,61,1);">Projects</span></p>
					  <p style="margin:10px 5px 0px 0px;text-align:Center;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-size:1.25em;color:rgba(78,78,78,1);">Fusce felis ipsum feugiat sed blandit scelerisque non sapien.</span></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-lg-block d-xl-block d-lg-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
		  </div>
		  <div class="ttr_index_html_row22 row" >
			 <div class="post_column col-xl-4 col-lg-4 col-md-6 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column220">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p style="margin: 20px;text-align:Center;line-height:1.25em;"><br style="font-family:'Open Sans','Arial';" /><span class="ttr_image" style="float:none;display:block;overflow:hidden;margin:0px 0px 0px 0px;padding:0px 0px 0px 0px;"></span><span class="ttr_image" style="float:none;display:block;overflow:hidden;margin:0px 0px 0px 0px;"><span><img style="max-height:205px;max-width:385px;" src="/sites/all/themes/realsteelbeagles/images/42.jpg" /></span></span></p>
					  <p style="margin: 20px;text-align:Center;line-height:1.25em;"><br style="font-family:'Open Sans','Arial';font-size:1.25em;color:rgba(78,78,78,1);" /></p>
					  <p style="margin:0px 0px 15px 0px;text-align:Center;line-height:1.25em;"><span style="font-family:'Open Sans','Arial';font-weight:700;font-size:1.25em;color:rgba(78,78,78,1);">Project 1</span></p>
					  <p style="margin:0px 5px 0px 0px;text-align:Center;line-height:1.25em;"><br style="font-family:'Open Sans','Arial';" /><span class="ttr_image" style="float:none;display:block;overflow:hidden;margin:0px 0px 0px 0px;"><span><img style="max-height:2px;max-width:50px;" src="/sites/all/themes/realsteelbeagles/images/43.png" /></span></span></p>
					  <p style="margin:20px 5px 5px 0px;text-align:Center;line-height:1.5em;"><span style="font-family:'Open Sans','Arial';font-size:0.938em;color:rgba(105,105,105,1);">Praesent vestibulum molestie nonummy hndrerit mauris hasellus porta.</span></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
			 <div class="post_column col-xl-4 col-lg-4 col-md-6 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column221">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p style="margin:5px 5px 5px 0px;text-align:Center;line-height:1.25em;"><br style="font-family:'Open Sans','Arial';" /><span class="ttr_image" style="float:none;display:block;overflow:hidden;margin:0px 0px 0px 0px;padding:0px 0px 0px 0px;"></span><span class="ttr_image" style="float:none;display:block;overflow:hidden;margin:0px 0px 0px 0px;"><span><img style="max-height:205px;max-width:385px;" src="/sites/all/themes/realsteelbeagles/images/44.jpg" /></span></span></p>
					  <p style="margin:5px 5px 5px 0px;text-align:Center;line-height:1.25em;"><br style="font-family:'Open Sans','Arial';font-size:1.25em;color:rgba(78,78,78,1);" /></p>
					  <p style="margin:0px 0px 15px 0px;text-align:Center;line-height:1.25em;"><span style="font-family:'Open Sans','Arial';font-weight:700;font-size:1.25em;color:rgba(78,78,78,1);">Project 2</span></p>
					  <p style="margin:5px 5px 0px 0px;text-align:Center;line-height:1.25em;"><br style="font-family:'Open Sans','Arial';" /><span class="ttr_image" style="float:none;display:block;overflow:hidden;margin:0px 0px 0px 0px;"><span><img style="max-height:2px;max-width:50px;" src="/sites/all/themes/realsteelbeagles/images/431.png" /></span></span></p>
					  <p style="margin:20px 5px 5px 0px;text-align:Center;line-height:1.5em;"><span style="font-family:'Open Sans','Arial';font-size:0.938em;color:rgba(105,105,105,1);">Praesent vestibulum molestie nonummy hndrerit mauris hasellus porta.</span></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
			 <div class="post_column col-xl-4 col-lg-4 col-md-6 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column222">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p style="margin:5px 5px 5px 0px;text-align:Center;line-height:1.25em;"><br style="font-family:'Open Sans','Arial';" /><span class="ttr_image" style="float:none;display:block;overflow:hidden;margin:0px 0px 0px 0px;padding:0px 0px 0px 0px;"></span><span class="ttr_image" style="float:none;display:block;overflow:hidden;margin:0px 0px 0px 0px;"><span><img style="max-height:205px;max-width:385px;" src="/sites/all/themes/realsteelbeagles/images/45.jpg" /></span></span></p>
					  <p style="margin:5px 5px 5px 0px;text-align:Center;line-height:1.25em;"><br style="font-family:'Open Sans','Arial';font-size:1.25em;color:rgba(78,78,78,1);" /></p>
					  <p style="margin:5px 0px 15px 0px;text-align:Center;line-height:1.25em;"><span style="font-family:'Open Sans','Arial';font-weight:700;font-size:1.25em;color:rgba(78,78,78,1);">Project 3</span></p>
					  <p style="margin:0px 5px 0px 0px;text-align:Center;line-height:1.25em;"><br style="font-family:'Open Sans','Arial';" /><span class="ttr_image" style="float:none;display:block;overflow:hidden;margin:0px 0px 0px 0px;"><span><img style="max-height:2px;max-width:50px;" src="/sites/all/themes/realsteelbeagles/images/432.png" /></span></span></p>
					  <p style="margin:20px 5px 5px 0px;text-align:Center;line-height:1.5em;"><span style="font-family:'Open Sans','Arial';font-size:0.938em;color:rgba(105,105,105,1);">Praesent vestibulum molestie nonummy hndrerit mauris hasellus porta.</span></p>
					  <p style="text-align:Center;"><br style="font-family:'Open Sans','Arial';" /></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-lg-block d-xl-block d-lg-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
			 <div class="post_column col-xl-4 col-lg-4 col-md-6 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column223">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p style="margin:5px 5px 5px 0px;text-align:Center;line-height:1.25em;"><br style="font-family:'Open Sans','Arial';" /><span class="ttr_image" style="float:none;display:block;overflow:hidden;margin:0px 0px 0px 0px;padding:0px 0px 0px 0px;"></span><span class="ttr_image" style="float:none;display:block;overflow:hidden;margin:0px 0px 0px 0px;"><span><img style="max-height:205px;max-width:385px;" src="/sites/all/themes/realsteelbeagles/images/46.jpg" /></span></span></p>
					  <p style="margin:5px 5px 5px 0px;text-align:Center;line-height:1.25em;"><br style="font-family:'Open Sans','Arial';font-size:1.25em;color:rgba(78,78,78,1);" /></p>
					  <p style="margin:5px 0px 15px 0px;text-align:Center;line-height:1.25em;"><span style="font-family:'Open Sans','Arial';font-weight:700;font-size:1.25em;color:rgba(78,78,78,1);">Project 4</span></p>
					  <p style="margin:5px 5px 0px 0px;text-align:Center;line-height:1.25em;"><br style="font-family:'Open Sans','Arial';" /><span class="ttr_image" style="float:none;display:block;overflow:hidden;margin:0px 0px 0px 0px;"><span><img style="max-height:2px;max-width:50px;" src="/sites/all/themes/realsteelbeagles/images/433.png" /></span></span><span style="font-family:'Open Sans','Arial';font-size:1em;color:rgba(78,78,78,1);"> </span></p>
					  <p style="margin:20px 5px 5px 0px;text-align:Center;line-height:1.5em;"><span style="font-family:'Open Sans','Arial';font-size:0.938em;color:rgba(105,105,105,1);">Praesent vestibulum molestie nonummy hndrerit mauris hasellus porta.</span></p>
					  <p style="margin:5px 5px 5px 5px;text-align:Center;"><br style="font-family:'Open Sans','Arial';" /></p>
					  <p style="text-align:Center;"><br style="font-family:'Open Sans','Arial';" /></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
			 <div class="post_column col-xl-4 col-lg-4 col-md-6 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column224">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p style="margin:5px 5px 5px 0px;text-align:Center;line-height:1.25em;"><br style="font-family:'Open Sans','Arial';" /><span class="ttr_image" style="float:none;display:block;overflow:hidden;margin:0px 0px 0px 0px;padding:0px 0px 0px 0px;"></span><span class="ttr_image" style="float:none;display:block;overflow:hidden;margin:0px 0px 0px 0px;"><span><img style="max-height:205px;max-width:385px;" src="/sites/all/themes/realsteelbeagles/images/47.jpg" /></span></span></p>
					  <p style="margin:5px 5px 5px 0px;text-align:Center;line-height:1.25em;"><br style="font-family:'Open Sans','Arial';font-size:1.25em;color:rgba(78,78,78,1);" /></p>
					  <p style="margin:5px 0px 15px 0px;text-align:Center;line-height:1.25em;"><span style="font-family:'Open Sans','Arial';font-weight:700;font-size:1.25em;color:rgba(78,78,78,1);">Project 5</span></p>
					  <p style="margin:5px 5px 0px 0px;text-align:Center;line-height:1.25em;"><br style="font-family:'Open Sans','Arial';" /><span class="ttr_image" style="float:none;display:block;overflow:hidden;margin:0px 0px 0px 0px;"><span><img style="max-height:2px;max-width:50px;" src="/sites/all/themes/realsteelbeagles/images/434.png" /></span></span></p>
					  <p style="margin:20px 5px 5px 0px;text-align:Center;line-height:1.5em;"><span style="font-family:'Open Sans','Arial';font-size:0.938em;color:rgba(105,105,105,1);">Praesent vestibulum molestie nonummy hndrerit mauris hasellus porta.</span></p>
					  <p style="margin:5px 5px 5px 0px;text-align:Center;"><br style="font-family:'Open Sans','Arial';" /></p>
					  <p style="margin:5px 5px 5px 5px;text-align:Center;"><br style="font-family:'Open Sans','Arial';" /></p>
					  <p style="text-align:Center;"><br style="font-family:'Open Sans','Arial';" /></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
			 <div class="post_column col-xl-4 col-lg-4 col-md-6 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column225">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p style="margin: 20px;text-align:Center;line-height:1.25em;"><br style="font-family:'Open Sans','Arial';" /><span class="ttr_image" style="float:none;display:block;overflow:hidden;margin:0px 0px 0px 0px;padding:0px 0px 0px 0px;"></span><span class="ttr_image" style="float:none;display:block;overflow:hidden;margin:0px 0px 0px 0px;"><span><img style="max-height:205px;max-width:385px;" src="/sites/all/themes/realsteelbeagles/images/48.jpg" /></span></span></p>
					  <p style="margin: 20px;text-align:Center;line-height:1.25em;"><br style="font-family:'Open Sans','Arial';font-size:1.25em;color:rgba(78,78,78,1);" /></p>
					  <p style="margin:0px 0px 15px 0px;text-align:Center;line-height:1.25em;"><span style="font-family:'Open Sans','Arial';font-weight:700;font-size:1.25em;color:rgba(78,78,78,1);">Project 6</span></p>
					  <p style="margin:0px 5px 0px 0px;text-align:Center;line-height:1.25em;"><br style="font-family:'Open Sans','Arial';" /><span class="ttr_image" style="float:none;display:block;overflow:hidden;margin:0px 0px 0px 0px;"><span><img style="max-height:2px;max-width:50px;" src="/sites/all/themes/realsteelbeagles/images/435.png" /></span></span></p>
					  <p style="margin:20px 5px 5px 0px;text-align:Center;line-height:1.5em;"><span style="font-family:'Open Sans','Arial';font-size:0.938em;color:rgba(105,105,105,1);">Praesent vestibulum molestie nonummy hndrerit mauris hasellus porta.</span></p>
					  <p style="text-align:Center;"><br style="font-family:'Open Sans','Arial';" /></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
		  </div>
		  <div class="ttr_index_html_row23 row" id="contact" >
			 <div class="post_column col-xl-12 col-lg-12 col-md-6 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column230">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p style="margin:5px 5px 5px 0px;text-align:Center;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-size:2.5em;color:rgba(231,76,61,1);">Contact</span></p>
					  <p style="margin:10px 5px 0px 0px;text-align:Center;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-size:1.25em;color:rgba(78,78,78,1);">Fusce felis ipsum feugiat sed blandit scelerisque non sapien.</span></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-lg-block d-xl-block d-lg-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
		  </div>
		  <div class="ttr_index_html_row24 row" >
			 <div class="post_column col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column240">
				   <div class="margin_collapsetop"></div>
				   <div class="html_content">
					  <p style="margin:5px 5px 5px 0px;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-weight:700;font-size:1.562em;color:rgba(78,78,78,1);">Get Touch With Us</span></p>
					  <p style="margin:20px 5px 5px 0px;line-height:1.56em;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-size:1.125em;color:rgba(78,78,78,1);">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</span></p>
					  <p style="margin:20px 5px 5px 0px;line-height:1.56em;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-size:0.938em;color:rgba(105,105,105,1);">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architectobeatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia.</span></p>
					  <p style="margin:5px 5px 5px 0px;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-size:1.125em;color:rgba(231,76,61,1);">The Company Name Inc.</span></p>
					  <p><br style="font-family:'Open Sans','Microsoft New Tai Lue';font-size:1em;color:rgba(105,105,105,1);" /></p>
					  <p style="margin:5px 5px 5px 0px;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-size:1em;color:rgba(78,78,78,1);">St Betant Place,</span></p>
					  <p style="margin:10px 5px 5px 0px;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-size:1em;color:rgba(105,105,105,1);">X Y Z 455 Fs 00.</span></p>
					  <p style="margin:10px 5px 5px 0px;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-size:1em;color:rgba(105,105,105,1);">Telephone: +1 8000 6003 6035</span></p>
					  <p style="margin:10px 5px 5px 0px;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-size:1em;color:rgba(105,105,105,1);">FAX: +1 800 779 8898</span></p>
					  <p style="margin:10px 5px 5px 0px;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-size:1em;color:rgba(105,105,105,1);">E-mail: mysite.com</span></p>
				   </div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
			 <div class="post_column col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12 col-12">
				<div class="ttr_index_html_column241">
				   <div class="margin_collapsetop"></div>
				   <div class="margin_collapsetop"></div>
				   <div style="clear:both;width:0px;"></div>
				</div>
			 </div>
			 <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;width:0px;"></div>
		  </div>
		  <div class="margin_collapsetop"></div>
	   </div>
	   <!--content_margin-->
	</div>
	<!--content-->
	<div style="clear:both"></div>
 </div> 
 </div> 
 <!-- end demo -->
  <div class="footer-widget-area">
    <div class="footer-widget-area_left_border_image">
      <div class="footer-widget-area_right_border_image">
        <div class="footer-widget-area_inner">
          <?php
            if( !empty($page['footerabovecolumn1'])|| !empty($page['footerabovecolumn2'])|| !empty($page['footerabovecolumn3'])||!empty($page['footerabovecolumn4'])):
            ?>
          <div class="ttr_footer-widget-area_inner_above_widget_container">
            <div class="ttr_footer-widget-area_inner_above0">
              <?php
                $showcolumn= !empty($page['footerabovecolumn1']);
                ?>
              <?php if($showcolumn): ?>
              <div class="cell1 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
                <div class="footerabovecolumn1">
                  <?php print render($page['footerabovecolumn1']); ?>
                </div>
              </div>
              <?php else: ?>
              <div class="cell1 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12"  style="background-color:transparent;">
                &nbsp;
              </div>
              <?php endif; ?>
              <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
              <?php
                $showcolumn= !empty($page['footerabovecolumn2']);
                ?>
              <?php if($showcolumn): ?>
              <div class="cell2 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
                <div class="footerabovecolumn2">
                  <?php print render($page['footerabovecolumn2']); ?>
                </div>
              </div>
              <?php else: ?>
              <div class="cell2 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12"  style="background-color:transparent;">
                &nbsp;
              </div>
              <?php endif; ?>
              <div class=" visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
              <?php
                $showcolumn= !empty($page['footerabovecolumn3']);
                ?>
              <?php if($showcolumn): ?>
              <div class="cell3 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
                <div class="footerabovecolumn3">
                  <?php print render($page['footerabovecolumn3']); ?>
                </div>
              </div>
              <?php else: ?>
              <div class="cell3 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12"  style="background-color:transparent;">
                &nbsp;
              </div>
              <?php endif; ?>
              <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
              <?php
                $showcolumn= !empty($page['footerabovecolumn4']);
                ?>
              <?php if($showcolumn): ?>
              <div class="cell4 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
                <div class="footerabovecolumn4">
                  <?php print render($page['footerabovecolumn4']); ?>
                </div>
              </div>
              <?php else: ?>
              <div class="cell4 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12"  style="background-color:transparent;">
                &nbsp;
              </div>
              <?php endif; ?>
              <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
              <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
            </div>
          </div>
          <div style="clear: both;"></div>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
  <footer id="ttr_footer">
    <div class="margin_collapsetop"></div>
    <div id="ttr_footer_inner">
               <div class="ttr_footer-widget-cell_inner_widget_container">
                  <div class="ttr_footer-widget-cell_inner0 container row">
                     <div class="post_column col-xl-1 col-lg-1 col-md-1 col-sm-3 col-xs-3 col-3">
                        <div class="footercellcolumn1">
                           <div class="margin_collapsetop"></div>
                           <div class="html_content"><br></div>
                           <div class="margin_collapsetop"></div>
                        </div>
                     </div>
                     <div class="post_column col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 col-12">
                        <div class="footercellcolumn2">
                           <div class="margin_collapsetop"></div>
                           <div class="html_content">
                              <p style="margin:0px 5px 5px 20px;"><br style="font-family:'Open Sans','Microsoft New Tai Lue';font-size:1.25em;color:rgba(255,255,255,1);"></p>
                              <p style="margin:0px 5px 5px 20px;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-weight:700;font-size:1.25em;color:rgba(255,255,255,1);">Few Words</span></p>
                              <p style="margin:10px 5px 5px 20px;"><span style="font-family:'Open Sans','Microsoft New Tai Lue';font-size:0.938em;color:rgba(157,157,157,1);">Aenean auctoi et urna.nc odio, gravida at, cursus nec, luctus a, Maecenas trisque orci em.uis ultricie Aenean auctoi et urna.nc odio, gravida at, cursus nec, luctus a, Maecenas trisque orci em.unean auctoi et urna.nc odio, gravida at, cursus nec, luctus a, Maecenas trisque orci em.</span></p>
                              <p style="margin:5px 5px 5px 20px;"><br style="font-family:'Open Sans','Microsoft New Tai Lue';"></p>
                           </div>
                           <div class="margin_collapsetop"></div>
                        </div>
                     </div>
                     <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;"></div>
                     <div class="post_column col-xl-2 col-lg-2 col-md-5 col-sm-12 col-xs-12 col-12">
                        <div class="footercellcolumn3">
                           <div class="margin_collapsetop"></div>
                           <div class="html_content">
                              <p style="margin:0px 5px 5px 20px;"><br style="font-family:'Open Sans','Arial';font-size:1.25em;color:rgba(255,255,255,1);"></p>
                              <p style="margin:0px 5px 5px 20px;"><span style="font-family:'Open Sans','Arial';font-weight:700;font-size:1.25em;color:rgba(255,255,255,1);">Other Links</span></p>
                              <p style="margin:10px 5px 5px 20px;"><a href="#" target="_self" class="tt_link"><span style="font-family:'Open Sans','Arial';font-size:0.938em;color:rgba(157,157,157,1);">Fashion</span></a></p>
                              <p style="margin:10px 5px 5px 20px;"><a href="#" target="_self" class="tt_link"><span style="font-family:'Open Sans','Arial';font-size:0.938em;color:rgba(157,157,157,1);">Real Steel</span></a></p>
                              <p style="margin:10px 5px 5px 20px;"><a href="#" target="_self" class="tt_link"><span style="font-family:'Open Sans','Arial';font-size:0.938em;color:rgba(157,157,157,1);">Best Song</span></a></p>
                              <p style="margin:10px 5px 5px 20px;"><a href="#" target="_self" class="tt_link"><span style="font-family:'Open Sans','Arial';font-size:0.938em;color:rgba(157,157,157,1);">News Feed</span></a></p>
                              <p style="margin:10px 5px 5px 20px;"><a href="#" target="_self" class="tt_link"><span style="font-family:'Open Sans','Arial';font-size:0.938em;color:rgba(157,157,157,1);">Drama Acts</span></a></p>
                              <p style="margin:10px 5px 5px 20px;"><a href="#" target="_self" class="tt_link"><span style="font-family:'Open Sans','Arial';font-size:0.938em;color:rgba(157,157,157,1);">Social Links</span></a></p>
                           </div>
                           <div class="margin_collapsetop"></div>
                        </div>
                     </div>
                     <div class=" visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;"></div>
                     <div class="post_column col-xl-1 col-lg-1 col-md-1 col-sm-3 col-xs-3 col-3">
                        <div class="footercellcolumn4">
                           <div class="margin_collapsetop"></div>
                           <div class="html_content"><br></div>
                           <div class="margin_collapsetop"></div>
                        </div>
                     </div>
                     <div class="post_column col-xl-2 col-lg-2 col-md-6 col-sm-12 col-xs-12 col-12">
                        <div class="footercellcolumn5">
                           <div class="margin_collapsetop"></div>
                           <div class="html_content">
                              <p style="margin:0px 5px 5px 20px;"><br style="font-family:'Open Sans','Arial';font-size:1.25em;color:rgba(255,255,255,1);"></p>
                              <p style="margin:0px 5px 5px 20px;"><span style="font-family:'Open Sans','Arial';font-weight:700;font-size:1.25em;color:rgba(255,255,255,1);">Usefull Links</span></p>
                              <p style="margin:10px 5px 5px 20px;"><a href="#" target="_self" class="tt_link"><span style="font-family:'Open Sans','Arial';font-size:0.938em;color:rgba(157,157,157,1);">Fashion</span></a></p>
                              <p style="margin:10px 5px 5px 20px;"><a href="#" target="_self" class="tt_link"><span style="font-family:'Open Sans','Arial';font-size:0.938em;color:rgba(157,157,157,1);">Real Steel</span></a></p>
                              <p style="margin:10px 5px 5px 20px;"><a href="#" target="_self" class="tt_link"><span style="font-family:'Open Sans','Arial';font-size:0.938em;color:rgba(157,157,157,1);">Best Song</span></a></p>
                              <p style="margin:10px 5px 5px 20px;"><a href="#" target="_self" class="tt_link"><span style="font-family:'Open Sans','Arial';font-size:0.938em;color:rgba(157,157,157,1);">News Feed</span></a></p>
                              <p style="margin:10px 5px 5px 20px;"><a href="#" target="_self" class="tt_link"><span style="font-family:'Open Sans','Arial';font-size:0.938em;color:rgba(157,157,157,1);">Drama Acts</span></a></p>
                              <p style="margin:10px 5px 5px 20px;"><a href="#" target="_self" class="tt_link"><span style="font-family:'Open Sans','Arial';font-size:0.938em;color:rgba(157,157,157,1);">Social Links</span></a></p>
                           </div>
                           <div class="margin_collapsetop"></div>
                        </div>
                     </div>
                     <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;"></div>
                     <div class="post_column col-xl-3 col-lg-3 col-md-5 col-sm-12 col-xs-12 col-12">
                        <div class="footercellcolumn6">
                           <div class="margin_collapsetop"></div>
                           <div class="html_content">
                              <p style="margin:0px 5px 5px 20px;"><br style="font-family:'Open Sans','Arial';font-size:1.25em;color:rgba(255,255,255,1);"></p>
                              <p style="margin:0px 5px 10px 20px;"><span style="font-family:'Open Sans','Arial';font-weight:700;font-size:1.25em;color:rgba(255,255,255,1);">Social Media Links</span></p>
                              <p style="margin:0px 5px 0px 20px;"><br style="font-family:'Open Sans','Arial';"><span class="ttr_image" style="float:Left;overflow:hidden;margin:0px 5px 0px 0px;"><span><img class="ttr_fill" style="height:28px;max-width:28px;" src="images/8.png"></span></span><span class="ttr_image" style="float:Left;overflow:hidden;margin:0px 5px 0px 0px;"><span><img class="ttr_fill" style="height:28px;max-width:28px;" src="images/9.png"></span></span><span class="ttr_image" style="float:Left;overflow:hidden;margin:0px 5px 0px 0px;"><span><img class="ttr_fill" style="height:28px;max-width:28px;" src="images/10.png"></span></span><span class="ttr_image" style="float:Left;overflow:hidden;margin:0px 0px 0px 0px;"><span><img class="ttr_fill" style="height:28px;max-width:28px;" src="images/11.png"></span></span></p>
                              <p style="margin:0px 5px 0px 20px;"><br style="font-family:'Open Sans','Arial';font-size:0.938em;color:rgba(157,157,157,1);"></p>
                              <p style="margin:0px 5px 0px 20px;"><br style="font-family:'Open Sans','Arial';font-size:0.938em;color:rgba(157,157,157,1);"></p>
                              <p style="margin:0px 5px 0px 20px;"><span style="font-family:'Open Sans','Arial';font-size:0.938em;color:rgba(157,157,157,1);">Mountain View, CA 943</span></p>
                              <p style="margin:0px 5px 5px 20px;"><span style="font-family:'Open Sans','Arial';font-size:0.938em;color:rgba(157,157,157,1);">United States of America</span></p>
                              <p style="margin:0px 5px 5px 0px;"><span style="font-family:'Open Sans','Arial';font-size:0.938em;color:rgba(157,157,157,1);">&nbsp;&nbsp;&nbsp;&nbsp; +3002 2554 23</span></p>
                              <p style="margin:0px 5px 5px 20px;"><br style="font-family:'Open Sans','Arial';font-size:0.938em;color:rgba(157,157,157,1);"></p>
                              <p style="margin:0px 5px 5px 20px;"><span style="font-family:'Open Sans','Arial';font-size:0.938em;color:rgba(77,76,77,1);">TemplateToaster Inc.</span></p>
                              <p style="margin:0px 5px 5px 20px;"><a href="#" target="_self" class="tt_link"><span style="font-family:'Open Sans','Arial';font-size:0.938em;color:rgba(157,157,157,1);">#</span></a></p>
                           </div>
                           <div class="margin_collapsetop"></div>
                        </div>
                     </div>
                     <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear: both;"></div>
                     <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
                  </div>
               </div>
               <div class="ttr_footer_bottom_footer">
                  <div class="ttr_footer_bottom_footer_inner">
                     <div class="ttr_footer_element_alignment container"></div>
                     <div id="ttr_footer_designed_by_links">
                        <a href="#" target="_self">
                        Website
                        </a>
                        <span id="ttr_footer_designed_by">
                        Designed With Bootstrap 3.3.7
                        </span>
                     </div>
                  </div>
               </div>
            </div>
  </footer>
  <div class="margin_collapsetop"></div>
  <div class="footer-widget-area">
    <?php
      if( !empty($page['footerbelowcolumn1'])|| !empty($page['footerbelowcolumn2'])|| !empty($page['footerbelowcolumn3'])||!empty($page['footerbelowcolumn4'])):
      ?>
    <div class="ttr_footer-widget-area_inner_below_widget_container">
      <div class="ttr_footer-widget-area_inner_below0">
        <?php
          $showcolumn= !empty($page['footerbelowcolumn1']);
          ?>
        <?php if($showcolumn): ?>
        <div class="cell1 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
          <div class="footerbelowcolumn1">
            <?php print render($page['footerbelowcolumn1']); ?>
          </div>
        </div>
        <?php else: ?>
        <div class="cell1 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12"  style="background-color:transparent;">
          &nbsp;
        </div>
        <?php endif; ?>
        <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
        <?php
          $showcolumn= !empty($page['footerbelowcolumn2']);
          ?>
        <?php if($showcolumn): ?>
        <div class="cell2 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
          <div class="footerbelowcolumn2">
            <?php print render($page['footerbelowcolumn2']); ?>
          </div>
        </div>
        <?php else: ?>
        <div class="cell2 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12"  style="background-color:transparent;">
          &nbsp;
        </div>
        <?php endif; ?>
        <div class=" visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
        <?php
          $showcolumn= !empty($page['footerbelowcolumn3']);
          ?>
        <?php if($showcolumn): ?>
        <div class="cell3 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
          <div class="footerbelowcolumn3">
            <?php print render($page['footerbelowcolumn3']); ?>
          </div>
        </div>
        <?php else: ?>
        <div class="cell3 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12"  style="background-color:transparent;">
          &nbsp;
        </div>
        <?php endif; ?>
        <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
        <?php
          $showcolumn= !empty($page['footerbelowcolumn4']);
          ?>
        <?php if($showcolumn): ?>
        <div class="cell4 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
          <div class="footerbelowcolumn4">
            <?php print render($page['footerbelowcolumn4']); ?>
          </div>
        </div>
        <?php else: ?>
        <div class="cell4 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12"  style="background-color:transparent;">
          &nbsp;
        </div>
        <?php endif; ?>
        <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
        <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
      </div>
    </div>
    <div style="clear: both;"></div>
    <?php endif; ?>
  </div>
</div>