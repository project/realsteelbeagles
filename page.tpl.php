<?php $theme_path = base_path() . path_to_theme(); ?>
<div class="ttr_banner_header">
  <?php
    if( !empty($page['headerabovecolumn1'])|| !empty($page['headerabovecolumn2'])|| !empty($page['headerabovecolumn3'])||!empty($page['headerabovecolumn4'])):
    ?>
  <div class="ttr_banner_header_inner_above_widget_container">
    <div class="ttr_banner_header_inner_above0">
      <?php
        $showcolumn= !empty($page['headerabovecolumn1']);
        ?>
      <?php if($showcolumn): ?>
      <div class="cell1 col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12  col-12">
        <div class="headerabovecolumn1">
          <?php print render($page['headerabovecolumn1']); ?>
        </div>
      </div>
      <?php endif; ?>
      <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
      <?php
        $showcolumn= !empty($page['headerabovecolumn2']);
        ?>
      <?php if($showcolumn): ?>
      <div class="cell2 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
        <div class="headerabovecolumn2">
          <?php print render($page['headerabovecolumn2']); ?>
        </div>
      </div>
      <?php endif; ?>
      <div class=" visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
      <?php
        $showcolumn= !empty($page['headerabovecolumn3']);
        ?>
      <?php if($showcolumn): ?>
      <div class="cell3 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
        <div class="headerabovecolumn3">
          <?php print render($page['headerabovecolumn3']); ?>
        </div>
      </div>
      <?php endif; ?>
      <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
      <?php
        $showcolumn= !empty($page['headerabovecolumn4']);
        ?>
      <?php if($showcolumn): ?>
      <div class="cell4 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
        <div class="headerabovecolumn4">
          <?php print render($page['headerabovecolumn4']); ?>
        </div>
      </div>
      <?php endif; ?>
      <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
      <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
    </div>
  </div>
  <div style="clear: both;"></div>
  <?php endif; ?>
</div>
<header id="ttr_header">
  <div class="margin_collapsetop"></div>
  <div id="ttr_header_inner">
    <div class="ttr_header_element_alignment container">
      <div class="ttr_images_container"></div>
    </div>
    <?php if ($logo): ?>
    <div class="ttr_header_logo"></div>
    <?php endif; ?>
  </div>
</header>
<div class="ttr_banner_header">
  <?php
    if( !empty($page['headerbelowcolumn1'])|| !empty($page['headerbelowcolumn2'])|| !empty($page['headerbelowcolumn3'])||!empty($page['headerbelowcolumn4'])):
    ?>
  <div class="ttr_banner_header_inner_below_widget_container">
    <div class="ttr_banner_header_inner_below0">
      <?php
        $showcolumn= !empty($page['headerbelowcolumn1']);
        ?>
      <?php if($showcolumn): ?>
      <div class="cell1 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
        <div class="headerbelowcolumn1">
          <?php print render($page['headerbelowcolumn1']); ?>
        </div>
      </div>
      <?php endif; ?>
      <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
      <?php
        $showcolumn= !empty($page['headerbelowcolumn2']);
        ?>
      <?php if($showcolumn): ?>
      <div class="cell2 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
        <div class="headerbelowcolumn2">
          <?php print render($page['headerbelowcolumn2']); ?>
        </div>
      </div>
      <?php endif; ?>
      <div class=" visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
      <?php
        $showcolumn= !empty($page['headerbelowcolumn3']);
        ?>
      <?php if($showcolumn): ?>
      <div class="cell3 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
        <div class="headerbelowcolumn3">
          <?php print render($page['headerbelowcolumn3']); ?>
        </div>
      </div>
      <?php endif; ?>
      <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
      <?php
        $showcolumn= !empty($page['headerbelowcolumn4']);
        ?>
      <?php if($showcolumn): ?>
      <div class="cell4 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
        <div class="headerbelowcolumn4">
          <?php print render($page['headerbelowcolumn4']); ?>
        </div>
      </div>
      <?php endif; ?>
      <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
      <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
    </div>
  </div>
  <div style="clear: both;"></div>
  <?php endif; ?>
</div>
<div class="ttr_banner_menu">
  <?php
    if( !empty($page['menuabovecolumn1'])|| !empty($page['menuabovecolumn2'])|| !empty($page['menuabovecolumn3'])||!empty($page['menuabovecolumn4'])):
    ?>
  <div class="ttr_banner_menu_inner_above_widget_container">
    <div class="ttr_banner_menu_inner_above0">
      <?php
        $showcolumn= !empty($page['menuabovecolumn1']);
        ?>
      <?php if($showcolumn): ?>
      <div class="cell1 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
        <div class="menuabovecolumn1">
          <?php print render($page['menuabovecolumn1']); ?>
        </div>
      </div>
      <?php endif; ?>
      <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
      <?php
        $showcolumn= !empty($page['menuabovecolumn2']);
        ?>
      <?php if($showcolumn): ?>
      <div class="cell2 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
        <div class="menuabovecolumn2">
          <?php print render($page['menuabovecolumn2']); ?>
        </div>
      </div>
      <?php endif; ?>
      <div class=" visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
      <?php
        $showcolumn= !empty($page['menuabovecolumn3']);
        ?>
      <?php if($showcolumn): ?>
      <div class="cell3 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
        <div class="menuabovecolumn3">
          <?php print render($page['menuabovecolumn3']); ?>
        </div>
      </div>
      <?php endif; ?>
      <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
      <?php
        $showcolumn= !empty($page['menuabovecolumn4']);
        ?>
      <?php if($showcolumn): ?>
      <div class="cell4 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
        <div class="menuabovecolumn4">
          <?php print render($page['menuabovecolumn4']); ?>
        </div>
      </div>
      <?php endif; ?>
      <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
      <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
    </div>
  </div>
  <div style="clear: both;"></div>
  <?php endif; ?>
</div>
<div id="ttr_menu">
  <div class="margin_collapsetop"></div>
  <nav class="navbar-default navbar-expand-md navbar">
    <div id="ttr_menu_inner_in">
      <div class="ttr_menu_element_alignment container"></div>
      <div class="ttr_images_container">
        <div class="ttr_menushape1">
          <div class="html_content">
            <p style="margin:5px 5px 5px 0px;"><a href="#" target="_self" class="tt_link" href="#" class="tt_link" target="_self"><span style="font-family:'Impact';font-weight:900;font-size:1.75em;color:rgba(56,55,56,1);">Real</span><span style="font-family:'Impact';font-weight:900;font-size:1.75em;color:rgba(0,0,255,1);"> </span><span style="font-family:'Impact';font-weight:900;font-size:1.75em;color:rgba(231,76,61,1);">Steel</span></a></p>
          </div>
        </div>
      </div>
      <div id="navigationmenu">
        <div class="navbar-header">
          <button id="nav-expander" data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
          <span class="ttr_menu_toggle_button">
          <span class="sr-only">
          </span>
          <span class="icon-bar">
          </span>
          <span class="icon-bar">
          </span>
          <span class="icon-bar">
          </span>
          </span>
          <span class="ttr_menu_button_text">
          Menu
          </span>
          </button>
        </div>
        <div class="menu-center collapse navbar-collapse">
          <ul class="ttr_menu_items nav navbar-nav navbar-right">
            <?php if ($main_menu){
              $menu = variable_get('menu_main_links_source', 'main-menu');
              echo generate_menu("ttr_", $menu,"menu");}?>
            <?php $theme_path = base_path() . path_to_theme(); ?>
          </ul>
        </div>
      </div>
    </div>
  </nav>
</div>
<div class="ttr_banner_menu">
  <?php
    if( !empty($page['menubelowcolumn1'])|| !empty($page['menubelowcolumn2'])|| !empty($page['menubelowcolumn3'])||!empty($page['menubelowcolumn4'])):
    ?>
  <div class="ttr_banner_menu_inner_below_widget_container">
    <div class="ttr_banner_menu_inner_below0">
      <?php
        $showcolumn= !empty($page['menubelowcolumn1']);
        ?>
      <?php if($showcolumn): ?>
      <div class="cell1 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
        <div class="menubelowcolumn1">
          <?php print render($page['menubelowcolumn1']); ?>
        </div>
      </div>
      <?php endif; ?>
      <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
      <?php
        $showcolumn= !empty($page['menubelowcolumn2']);
        ?>
      <?php if($showcolumn): ?>
      <div class="cell2 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
        <div class="menubelowcolumn2">
          <?php print render($page['menubelowcolumn2']); ?>
        </div>
      </div>
      <?php endif; ?>
      <div class=" visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
      <?php
        $showcolumn= !empty($page['menubelowcolumn3']);
        ?>
      <?php if($showcolumn): ?>
      <div class="cell3 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
        <div class="menubelowcolumn3">
          <?php print render($page['menubelowcolumn3']); ?>
        </div>
      </div>
      <?php endif; ?>
      <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
      <?php
        $showcolumn= !empty($page['menubelowcolumn4']);
        ?>
      <?php if($showcolumn): ?>
      <div class="cell4 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
        <div class="menubelowcolumn4">
          <?php print render($page['menubelowcolumn4']); ?>
        </div>
      </div>
      <?php endif; ?>
      <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
      <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
    </div>
  </div>
  <div style="clear: both;"></div>
  <?php endif; ?>
</div>
<div class="ttr_banner_slideshow">
  <?php
    if( !empty($page['slideshowabovecolumn1'])|| !empty($page['slideshowabovecolumn2'])|| !empty($page['slideshowabovecolumn3'])||!empty($page['slideshowabovecolumn4'])):
    ?>
  <div class="ttr_banner_slideshow_inner_above_widget_container">
    <div class="ttr_banner_slideshow_inner_above0">
      <?php
        $showcolumn= !empty($page['slideshowabovecolumn1']);
        ?>
      <?php if($showcolumn): ?>
      <div class="cell1 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
        <div class="slideshowabovecolumn1">
          <?php print render($page['slideshowabovecolumn1']); ?>
        </div>
      </div>
      <?php endif; ?>
      <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
      <?php
        $showcolumn= !empty($page['slideshowabovecolumn2']);
        ?>
      <?php if($showcolumn): ?>
      <div class="cell2 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
        <div class="slideshowabovecolumn2">
          <?php print render($page['slideshowabovecolumn2']); ?>
        </div>
      </div>
      <?php endif; ?>
      <div class=" visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
      <?php
        $showcolumn= !empty($page['slideshowabovecolumn3']);
        ?>
      <?php if($showcolumn): ?>
      <div class="cell3 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
        <div class="slideshowabovecolumn3">
          <?php print render($page['slideshowabovecolumn3']); ?>
        </div>
      </div>
      <?php endif; ?>
      <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
      <?php
        $showcolumn= !empty($page['slideshowabovecolumn4']);
        ?>
      <?php if($showcolumn): ?>
      <div class="cell4 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
        <div class="slideshowabovecolumn4">
          <?php print render($page['slideshowabovecolumn4']); ?>
        </div>
      </div>
      <?php endif; ?>
      <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
      <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
    </div>
  </div>
  <div style="clear: both;"></div>
  <?php endif; ?>
</div>
<div class="ttr_slideshow">
  <div class="margin_collapsetop"></div>
  <div id="ttr_slideshow_inner">
    <ul>
      <li id="Slide0" class="ttr_slide" data-slideEffect="SlideRight">
        <a href="#"></a>
        <div class="ttr_slideshow_last">
          <div class="ttr_slideshow_element_alignment container" data-begintime="0" data-effect="SlideLeft" data-easing="linear" data-slide="None" data-duration="0"></div>
          <div class="ttr_slideshowshape01" data-effect="None" data-begintime="0" data-duration="1" data-easing="linear" data-slide="None">
            <div class="html_content">
              <p style="margin:5px 5px 5px 0px;text-align:Center;line-height: normal;"><span style="font-family:'Open Sans','Arial';font-weight:700;font-size:4.5em;color:rgba(255,255,255,1);">A Large </span><span style="font-family:'Open Sans','Arial';font-weight:700;font-size:4.5em;color:rgba(231,76,61,1);">ASSORTMENT</span><span style="font-family:'Open Sans','Arial';font-weight:700;font-size:4.5em;color:rgba(255,255,255,1);"> of Steel Products</span></p>
            </div>
          </div>
        </div>
      </li>
      <li id="Slide1" class="ttr_slide" data-slideEffect="Wipe">
        <a href="#"></a>
        <div class="ttr_slideshow_last">
          <div class="ttr_slideshow_element_alignment container" data-begintime="0" data-effect="SlideLeft" data-easing="linear" data-slide="None" data-duration="0"></div>
          <div class="ttr_slideshowshape11" data-effect="None" data-begintime="0" data-duration="1" data-easing="linear" data-slide="None">
            <div class="html_content">
              <p style="text-align:Center;line-height: normal;"><span style="font-family:'Open Sans','Arial';font-weight:700;font-size:4.5em;color:rgba(255,255,255,1);">Maecenas tristiqu </span><span style="font-family:'Open Sans','Arial';font-weight:700;font-size:4.5em;color:rgba(231,76,61,1);">ULTRICIES</span><span style="font-family:'Open Sans','Arial';font-weight:700;font-size:4.5em;color:rgba(255,255,255,1);"> pharetra magna.</span></p>
            </div>
          </div>
        </div>
      </li>
      <li id="Slide2" class="ttr_slide" data-slideEffect="SlideLeft">
        <a href="#"></a>
        <div class="ttr_slideshow_last">
          <div class="ttr_slideshow_element_alignment container" data-begintime="0" data-effect="SlideLeft" data-easing="linear" data-slide="None" data-duration="0"></div>
          <div class="ttr_slideshowshape21" data-effect="None" data-begintime="0" data-duration="1" data-easing="linear" data-slide="None">
            <div class="html_content">
              <p style="text-align:Center;line-height: normal;"><span style="font-family:'Open Sans','Calibri';font-weight:700;font-size:4.5em;color:rgba(255,255,255,1);">Fusce felis ipsum </span><span style="font-family:'Open Sans','Calibri';font-weight:700;font-size:4.5em;color:rgba(231,76,61,1);">FEUGIAT</span><span style="font-family:'Open Sans','Calibri';font-weight:700;font-size:4.5em;color:rgba(255,255,255,1);"> sed blandit</span></p>
            </div>
          </div>
        </div>
      </li>
    </ul>
  </div>
  <div class="ttr_slideshow_in">
    <div class="ttr_slideshow_last">
      <div id="nav"></div>
      <div class="ttr_slideshow_logo"></div>
    </div>
  </div>
</div>
<div class="ttr_banner_slideshow">
  <?php
    if( !empty($page['slideshowbelowcolumn1'])|| !empty($page['slideshowbelowcolumn2'])|| !empty($page['slideshowbelowcolumn3'])||!empty($page['slideshowbelowcolumn4'])):
    ?>
  <div class="ttr_banner_slideshow_inner_below_widget_container">
    <div class="ttr_banner_slideshow_inner_below0">
      <?php
        $showcolumn= !empty($page['slideshowbelowcolumn1']);
        ?>
      <?php if($showcolumn): ?>
      <div class="cell1 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
        <div class="slideshowbelowcolumn1">
          <?php print render($page['slideshowbelowcolumn1']); ?>
        </div>
      </div>
      <?php endif; ?>
      <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
      <?php
        $showcolumn= !empty($page['slideshowbelowcolumn2']);
        ?>
      <?php if($showcolumn): ?>
      <div class="cell2 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
        <div class="slideshowbelowcolumn2">
          <?php print render($page['slideshowbelowcolumn2']); ?>
        </div>
      </div>
      <?php endif; ?>
      <div class=" visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
      <?php
        $showcolumn= !empty($page['slideshowbelowcolumn3']);
        ?>
      <?php if($showcolumn): ?>
      <div class="cell3 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
        <div class="slideshowbelowcolumn3">
          <?php print render($page['slideshowbelowcolumn3']); ?>
        </div>
      </div>
      <?php endif; ?>
      <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
      <?php
        $showcolumn= !empty($page['slideshowbelowcolumn4']);
        ?>
      <?php if($showcolumn): ?>
      <div class="cell4 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
        <div class="slideshowbelowcolumn4">
          <?php print render($page['slideshowbelowcolumn4']); ?>
        </div>
      </div>
      <?php endif; ?>
      <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
      <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
    </div>
  </div>
  <div style="clear: both;"></div>
  <?php endif; ?>
</div>
<div class="margin_collapsetop"></div>
<div id="ttr_page" class="container">
  <div id="ttr_content_and_sidebar_container">
    <div id="ttr_content">
      <div id="ttr_content_margin" class="container-fluid">
        <div class="margin_collapsetop"></div>
        <?php if ($breadcrumb): ?>
        <?php print $breadcrumb; ?>
        <?php endif; ?>
        <?php
          if( !empty($page['contenttopcolumn1'])|| !empty($page['contenttopcolumn2'])|| !empty($page['contenttopcolumn3'])||!empty($page['contenttopcolumn4'])):
          ?>
        <div class="ttr_topcolumn_widget_container">
          <div class="contenttopcolumn0">
            <?php
              $showcolumn= !empty($page['contenttopcolumn1']);
              ?>
            <?php if($showcolumn): ?>
            <div class="cell1 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
              <div class="topcolumn1">
                <?php print render($page['contenttopcolumn1']); ?>
              </div>
            </div>
            <?php else: ?>
            <div class="cell1 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12"  style="background-color:transparent;">
              &nbsp;
            </div>
            <?php endif; ?>
            <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
            <?php
              $showcolumn= !empty($page['contenttopcolumn2']);
              ?>
            <?php if($showcolumn): ?>
            <div class="cell2 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
              <div class="topcolumn2">
                <?php print render($page['contenttopcolumn2']); ?>
              </div>
            </div>
            <?php else: ?>
            <div class="cell2 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12"  style="background-color:transparent;">
              &nbsp;
            </div>
            <?php endif; ?>
            <div class=" visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
            <?php
              $showcolumn= !empty($page['contenttopcolumn3']);
              ?>
            <?php if($showcolumn): ?>
            <div class="cell3 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
              <div class="topcolumn3">
                <?php print render($page['contenttopcolumn3']); ?>
              </div>
            </div>
            <?php else: ?>
            <div class="cell3 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12"  style="background-color:transparent;">
              &nbsp;
            </div>
            <?php endif; ?>
            <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
            <?php
              $showcolumn= !empty($page['contenttopcolumn4']);
              ?>
            <?php if($showcolumn): ?>
            <div class="cell4 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
              <div class="topcolumn4">
                <?php print render($page['contenttopcolumn4']); ?>
              </div>
            </div>
            <?php else: ?>
            <div class="cell4 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12"  style="background-color:transparent;">
              &nbsp;
            </div>
            <?php endif; ?>
            <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
            <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
          </div>
        </div>
        <div style="clear: both;"></div>
        <?php endif; ?>
        <div style="clear:both;"></div>
        <?php if ($tabs): ?>
        <div style="clear:both;" class="tabs"><?php print render($tabs); ?></div>
        <?php endif; ?>
        <?php print render($page['help']); ?>
        <?php print $messages; ?>
        <?php if ($action_links): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
        <?php endif; ?>
        <div class="row">
          <?php print render($page['content']); ?>
        </div>
        <?php
          if( !empty($page['contentbottomcolumn1'])|| !empty($page['contentbottomcolumn2'])|| !empty($page['contentbottomcolumn3'])||!empty($page['contentbottomcolumn4'])):
          ?>
        <div class="ttr_bottomcolumn_widget_container">
          <div class="contentbottomcolumn0">
            <?php
              $showcolumn= !empty($page['contentbottomcolumn1']);
              ?>
            <?php if($showcolumn): ?>
            <div class="cell1 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
              <div class="bottomcolumn1">
                <?php print render($page['contentbottomcolumn1']); ?>
              </div>
            </div>
            <?php else: ?>
            <div class="cell1 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12"  style="background-color:transparent;">
              &nbsp;
            </div>
            <?php endif; ?>
            <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
            <?php
              $showcolumn= !empty($page['contentbottomcolumn2']);
              ?>
            <?php if($showcolumn): ?>
            <div class="cell2 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
              <div class="bottomcolumn2">
                <?php print render($page['contentbottomcolumn2']); ?>
              </div>
            </div>
            <?php else: ?>
            <div class="cell2 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12"  style="background-color:transparent;">
              &nbsp;
            </div>
            <?php endif; ?>
            <div class=" visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
            <?php
              $showcolumn= !empty($page['contentbottomcolumn3']);
              ?>
            <?php if($showcolumn): ?>
            <div class="cell3 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
              <div class="bottomcolumn3">
                <?php print render($page['contentbottomcolumn3']); ?>
              </div>
            </div>
            <?php else: ?>
            <div class="cell3 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12"  style="background-color:transparent;">
              &nbsp;
            </div>
            <?php endif; ?>
            <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
            <?php
              $showcolumn= !empty($page['contentbottomcolumn4']);
              ?>
            <?php if($showcolumn): ?>
            <div class="cell4 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
              <div class="bottomcolumn4">
                <?php print render($page['contentbottomcolumn4']); ?>
              </div>
            </div>
            <?php else: ?>
            <div class="cell4 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12"  style="background-color:transparent;">
              &nbsp;
            </div>
            <?php endif; ?>
            <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
            <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
          </div>
        </div>
        <div style="clear: both;"></div>
        <?php endif; ?>
        <div class="margin_collapsetop"></div>
      </div>
    </div>
    <div style="clear:both;"></div>
  </div>
  <div class="footer-widget-area">
    <div class="footer-widget-area_left_border_image">
      <div class="footer-widget-area_right_border_image">
        <div class="footer-widget-area_inner">
          <?php
            if( !empty($page['footerabovecolumn1'])|| !empty($page['footerabovecolumn2'])|| !empty($page['footerabovecolumn3'])||!empty($page['footerabovecolumn4'])):
            ?>
          <div class="ttr_footer-widget-area_inner_above_widget_container">
            <div class="ttr_footer-widget-area_inner_above0">
              <?php
                $showcolumn= !empty($page['footerabovecolumn1']);
                ?>
              <?php if($showcolumn): ?>
              <div class="cell1 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
                <div class="footerabovecolumn1">
                  <?php print render($page['footerabovecolumn1']); ?>
                </div>
              </div>
              <?php else: ?>
              <div class="cell1 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12"  style="background-color:transparent;">
                &nbsp;
              </div>
              <?php endif; ?>
              <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
              <?php
                $showcolumn= !empty($page['footerabovecolumn2']);
                ?>
              <?php if($showcolumn): ?>
              <div class="cell2 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
                <div class="footerabovecolumn2">
                  <?php print render($page['footerabovecolumn2']); ?>
                </div>
              </div>
              <?php else: ?>
              <div class="cell2 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12"  style="background-color:transparent;">
                &nbsp;
              </div>
              <?php endif; ?>
              <div class=" visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
              <?php
                $showcolumn= !empty($page['footerabovecolumn3']);
                ?>
              <?php if($showcolumn): ?>
              <div class="cell3 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
                <div class="footerabovecolumn3">
                  <?php print render($page['footerabovecolumn3']); ?>
                </div>
              </div>
              <?php else: ?>
              <div class="cell3 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12"  style="background-color:transparent;">
                &nbsp;
              </div>
              <?php endif; ?>
              <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
              <?php
                $showcolumn= !empty($page['footerabovecolumn4']);
                ?>
              <?php if($showcolumn): ?>
              <div class="cell4 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
                <div class="footerabovecolumn4">
                  <?php print render($page['footerabovecolumn4']); ?>
                </div>
              </div>
              <?php else: ?>
              <div class="cell4 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12"  style="background-color:transparent;">
                &nbsp;
              </div>
              <?php endif; ?>
              <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
              <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
            </div>
          </div>
          <div style="clear: both;"></div>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
  <footer id="ttr_footer">
    <div class="margin_collapsetop"></div>
    <div id="ttr_footer_inner">
      <div id="ttr_footer_top_for_widgets">
        <div  class="ttr_footer_top_for_widgets_inner">
          <?php
            if (   !empty($page['leftfooterarea']) || !empty($page['centerfooterarea']) || !empty($page['rightfooterarea'])):
            ?>
          <div class="footer-widget-area_fixed">
            <div style="margin:0 auto;">
              <?php if(!empty($page['leftfooterarea'])): ?>
              <div  id="first" class="widget-area  col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12 col-12">
                <?php print render($page['leftfooterarea']); ?>
              </div>
              <div class="visible-xs d-block" style="clear:both;"></div>
              <?php else: ?>
              <div  id="first" class="widget-area  col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12 col-12">
                &nbsp;
              </div>
              <div class="visible-xs d-block" style="clear:both;"></div>
              <?php endif; ?>
              <?php if(!empty($page['centerfooterarea'])): ?>
              <div  id="second" class="widget-area  col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12 col-12">
                <?php print render($page['centerfooterarea']); ?>
              </div>
              <div class="visible-xs d-block" style="clear:both;"></div>
              <?php else: ?>
              <div  id="second" class="widget-area  col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12 col-12">
                &nbsp;
              </div>
              <div class="visible-xs d-block" style="clear:both;"></div>
              <?php endif; ?>
              <?php if(!empty($page['rightfooterarea'])): ?>
              <div  id="third" class="widget-area  col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12 col-12">
                <?php print render($page['rightfooterarea']); ?>
              </div>
              <div class="visible-lg visible-md visible-sm visible-xs d-xl-block d-lg-block d-md-block d-sm-block d-block" style="clear:both;"></div>
              <?php else: ?>
              <div  id="third" class="widget-area  col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12 col-12">
                &nbsp;
              </div>
              <div class=" visible-lg visible-md visible-sm visible-xs d-xl-block d-lg-block d-md-block d-sm-block d-block" style="clear:both;"></div>
              <?php endif; ?>
            </div>
          </div>
          <?php endif; ?>
        </div>
      </div>
      <div class="ttr_footer_bottom_footer">
        <div class="ttr_footer_bottom_footer_inner">
          <div class="ttr_footer_element_alignment container"></div>
          <div id="ttr_footer_designed_by_links">
            <a href="#" target="_self"> Drupal Theme </a>
            <span id="ttr_footer_designed_by"> Designed With Bootstrap 3.3.7 </span>
          </div>
          <?php  print $feed_icons;   ?>
          <a href="#" class="ttr_footer_facebook " target="_self" >
          </a>
          <a href="#" class="ttr_footer_linkedin " target="_self" >
          </a>
          <a href="#" class="ttr_footer_twitter " target="_self" >
          </a>
          <a href="#" class="ttr_footer_instagram " target="_self" >
          </a>
        </div>
      </div>
    </div>
  </footer>
  <div class="margin_collapsetop"></div>
  <div class="footer-widget-area">
    <?php
      if( !empty($page['footerbelowcolumn1'])|| !empty($page['footerbelowcolumn2'])|| !empty($page['footerbelowcolumn3'])||!empty($page['footerbelowcolumn4'])):
      ?>
    <div class="ttr_footer-widget-area_inner_below_widget_container">
      <div class="ttr_footer-widget-area_inner_below0">
        <?php
          $showcolumn= !empty($page['footerbelowcolumn1']);
          ?>
        <?php if($showcolumn): ?>
        <div class="cell1 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
          <div class="footerbelowcolumn1">
            <?php print render($page['footerbelowcolumn1']); ?>
          </div>
        </div>
        <?php else: ?>
        <div class="cell1 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12"  style="background-color:transparent;">
          &nbsp;
        </div>
        <?php endif; ?>
        <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
        <?php
          $showcolumn= !empty($page['footerbelowcolumn2']);
          ?>
        <?php if($showcolumn): ?>
        <div class="cell2 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
          <div class="footerbelowcolumn2">
            <?php print render($page['footerbelowcolumn2']); ?>
          </div>
        </div>
        <?php else: ?>
        <div class="cell2 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12"  style="background-color:transparent;">
          &nbsp;
        </div>
        <?php endif; ?>
        <div class=" visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
        <?php
          $showcolumn= !empty($page['footerbelowcolumn3']);
          ?>
        <?php if($showcolumn): ?>
        <div class="cell3 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
          <div class="footerbelowcolumn3">
            <?php print render($page['footerbelowcolumn3']); ?>
          </div>
        </div>
        <?php else: ?>
        <div class="cell3 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12"  style="background-color:transparent;">
          &nbsp;
        </div>
        <?php endif; ?>
        <div class=" visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
        <?php
          $showcolumn= !empty($page['footerbelowcolumn4']);
          ?>
        <?php if($showcolumn): ?>
        <div class="cell4 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12">
          <div class="footerbelowcolumn4">
            <?php print render($page['footerbelowcolumn4']); ?>
          </div>
        </div>
        <?php else: ?>
        <div class="cell4 col-xl-3 col-lg-3 col-md-6 col-sm-12  col-xs-12  col-12"  style="background-color:transparent;">
          &nbsp;
        </div>
        <?php endif; ?>
        <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
        <div class=" visible-lg-block d-xl-block d-lg-block visible-md-block d-md-block visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>
      </div>
    </div>
    <div style="clear: both;"></div>
    <?php endif; ?>
  </div>
</div>