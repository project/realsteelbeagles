<?php
global $justify;
global $magmenu;
global $vmagmenu;
global $menuh;
global $vmenuh;
global $smenuh;
global $ocmenu;
if (!function_exists('generate_menu')){
function generate_menu($cssprefix="~prefix~",$menu, $menutype) {
    $pid = $menu;
    $tree = menu_tree_all_data($pid);
    $count=count($tree);
    $i =1;
    $output='';
    
    global $ocmenu;
    global $justify;
    
    foreach($tree as $tree_item)
    {
        if ($tree_item['link']['hidden'] == 0)
        {
            if($tree_item['link']['has_children'])
            {
                if( menu_get_active_title()==$tree_item['link']['title'])

                    $output.='<li class="'.$cssprefix.$menutype.'_items_parent dropdown"><a href="'. check_url(url($tree_item['link']['href'], $tree_item['link']['localized_options'])).'" class="'.$cssprefix.$menutype.'_items_parent_link_active_arrow dropdown-toggle" data-toggle="dropdown" ><span class="menuchildicon"></span>' . $tree_item['link']['title'] ;
                else
                    $output.='<li class="'.$cssprefix.$menutype.'_items_parent dropdown"><a href="'. check_url(url($tree_item['link']['href'], $tree_item['link']['localized_options'])).'" class="'.$cssprefix.$menutype.'_items_parent_link_arrow dropdown-toggle" data-toggle="dropdown" ><span class="menuchildicon"></span>' . $tree_item['link']['title'] ;

                if($i != $count)
                {
				            if($justify)
				            {
                              $output.='<hr class="horiz_separator" /></a>';
				          	}
				          	else
					          {
					            $output.='</a><hr class="horiz_separator" />';
					          }
                }
                else
                {
                    $output.='</a>';
                }

                $output.=generate_level1_children($tree_item['below']);
                $output.='</li>';
            }
            else {
                if( menu_get_active_title()==$tree_item['link']['title'])
                {
                    $output.='<li class="'.$cssprefix.$menutype.'_items_parent dropdown active"><a href="' .check_url(url($tree_item['link']['href'], $tree_item['link']['localized_options'])). '" class="'.$cssprefix.$menutype.'_items_parent_link_active"><span class="menuchildicon"></span>' . $tree_item['link']['title'] ;
                   
                   if($i != $count)
                    {
                        if($justify)
				                {
                          $output.='<hr class="horiz_separator" /></a>';
					              }
					              else
					              {
					                $output.='</a><hr class="horiz_separator" />';
					              }
                    }
                   else
                   {
                       $output.='</a>';
                   }

                    $output.='</li>';
                }
                else
                {
                    $output.='<li class="'.$cssprefix.$menutype.'_items_parent dropdown"><a href="' .check_url(url($tree_item['link']['href'], $tree_item['link']['localized_options'])). '" class="'.$cssprefix.$menutype.'_items_parent_link"><span class="menuchildicon"></span>' . $tree_item['link']['title'] ;
                   
                   if($i != $count)
                   {
                        if($justify)
				                 {
                           $output.='<hr class="horiz_separator" /></a>';
					               }
					               else
					               {
					                  $output.='</a><hr class="horiz_separator" />';
					               }
                    }
                   else
                   {
                       $output.='</a>';
                   }


                    $output.='</li>';
                }
            }
        }

        $i++;

    }

    return $output;
}

function generate_level1_children($tree)
{
    global $magmenu;
    global $menuh;
    global $smenuh;

    $count=count($tree);
    $output='';
    $output.='<ul class="child dropdown-menu" role="menu">';
    $i =1;
    foreach($tree as $tree_item)
    {
        // print_r($tree_item);
        if ($tree_item['link']['hidden'] == 0)
        {
            if($tree_item['link']['has_children'])
            {

                if($magmenu)
                {
                    $output.= '<li class="span1 unstyled dropdown dropdown-submenu">';
                    $output.='<a class="subchild dropdown-toggle" data-toggle="dropdown" href="' . check_url(url($tree_item['link']['href'], $tree_item['link']['localized_options'])) . '"><span class="menuchildicon"></span>' .  $tree_item['link']['link_title']  . '</a>';
                    if ($magmenu)
                    {
                        $output.='<hr class="separator" />';
                    }
                    else
                    {
                        if($i != $count)
                        {
                            $output.='<hr class="separator" />';
                        }
                    }

                }
                else
                {
                    $output.='<li class ="dropdown dropdown-submenu"><a class="subchild dropdown-toggle" data-toggle="dropdown" href="' . check_url(url($tree_item['link']['href'], $tree_item['link']['localized_options'])) . '"><span class="menuchildicon"></span>' .  $tree_item['link']['link_title'] . '</a>';
                    if ($magmenu)
                    {
                        $output.='<hr class="separator" />';
                    }
                    else
                    {
                        if($i != $count)
                        {
                            $output.='<hr class="separator" />';
                        }
                    }
                }

                if($magmenu)
                {
                    $output.= generate_level2_children($tree_item['below']);
                    $output.= '</li>';
                }
                else
                {
                    $output.= generate_level2_children($tree_item['below']);
                    $output.= '</li>';
                }
            }
            else
            {
                $output.= '<li><a href="' . check_url(url($tree_item['link']['href'], $tree_item['link']['localized_options'])).  '"><span class="menuchildicon"></span>' .   $tree_item['link']['link_title'] .  '</a>';
                if ($magmenu)
                {
                    $output.='<hr class="separator" />';
                }
                else
                {
                    if($i != $count)
                    {
                        $output.='<hr class="separator" />';
                    }
                }
                $output.='</li>';

            }
        }
        $i++;
    }

    $output.='</ul>';
    return $output;
}

function generate_level2_children($tree)
{
    global $magmenu;
    global $menuh;
    $count=count($tree);
    $output='';

    if($magmenu)
    {
        $output.='<ul role="menu">';
    }
    else
    {
        if($menuh)
        {
            $output.='<ul class="sub-menu" role="menu">';
        }

        else
        {
            $output.='<ul class="dropdown-menu sub-menu menu-dropdown-styles" role="menu">';
        }

    }

    $i = 1;
    foreach($tree as $tree_item)
    {
        if ($tree_item['link']['hidden'] == 0)
        {
            if($tree_item['link']['has_children'])
            {
                $output.='<li class="dropdown dropdown-submenu"><a class="subchild dropdown-toggle" data-toggle="dropdown" href="' . check_url(url($tree_item['link']['href'], $tree_item['link']['localized_options'])) . '"><span class="menuchildicon"></span>' . $tree_item['link']['link_title'] . '</a>';
                if ($magmenu)
                {
                    $output.='<hr class="separator" />';
                }
                else
                {
                    if($i != $count)
                    {
                        $output.='<hr class="separator" />';
                    }
                }


                $output.= generate_level2_children($tree_item['below']);
                $output.='</li>';

            }
            else
            {
                $output.='<li><a href="' . check_url(url($tree_item['link']['href'], $tree_item['link']['localized_options'])) . '"><span class="menuchildicon"></span>' . $tree_item['link']['link_title'] . '</a>';
                if ($magmenu)
                {
                    $output.='<hr class="separator" />';
                }
                else
                {
                    if($i != $count)
                    {
                        $output.='<hr class="separator" />';
                    }
                }
                $output.='</li>';
            }
        }
        $i++;
    }
    $output.='</ul>';
    return $output;
}
}
/*function user_menu_generate_menu($cssprefix="~prefix~",$menu_name) {
    $pid = variable_get('secondary_links_source',$menu_name);
    $tree = menu_tree_all_data($pid);
    $count=count($tree);
    $i =1;
    $output='';
    global $ocmenu;

    foreach($tree as $tree_item)
    {
        if ($tree_item['link']['hidden'] == 0)
        {
            if($tree_item['link']['has_children'])
            {
                if( menu_get_active_title()==$tree_item['link']['title'])

                    $output.='<li class="'.$cssprefix.'vmenu_items_parent dropdown" style="float:none"><a href="'. check_url(url($tree_item['link']['href'], $tree_item['link']['localized_options'])).'" class="'.$cssprefix.'vmenu_items_parent_link_active_arrow"><span class="menuchildicon"></span>' . $tree_item['link']['title'] . '</a>';
                else
                    $output.='<li class="'.$cssprefix.'vmenu_items_parent dropdown" style="float:none"><a href="'. check_url(url($tree_item['link']['href'], $tree_item['link']['localized_options'])).'" class="'.$cssprefix.'vmenu_items_parent_link_arrow dropdown-toggle" data-toggle="dropdown" ><span class="menuchildicon"></span>' . $tree_item['link']['title'] . '</a>';

                if($i != $count)
                {
                    $output.='<hr class="horiz_separator" />';
                }
                $output.= user_menu_generate_level1_children($tree_item['below']);
                $output.='</li>';
            }
            else {
                if( menu_get_active_title()==$tree_item['link']['title'])
                {
                    $output.='<li class="'.$cssprefix.'vmenu_items_parent dropdown active" style="float:none"><a href="' .check_url(url($tree_item['link']['href'], $tree_item['link']['localized_options'])). '" class="'.$cssprefix.'vmenu_items_parent_link_active"><span class="menuchildicon"></span>' . $tree_item['link']['title'] . '</a>';
                    if($i != $count)
                    {
                        $output.='<hr class="horiz_separator" />';
                    }
                    $output.='</li>';
                }
                else
                {
                    $output.='<li class="'.$cssprefix.'vmenu_items_parent dropdown" style="float:none"><a href="' .check_url(url($tree_item['link']['href'], $tree_item['link']['localized_options'])). '" class="'.$cssprefix.'vmenu_items_parent_link"><span class="menuchildicon"></span>' . $tree_item['link']['title'] . '</a>';
                    if($i != $count)
                    {
                        $output.='<hr class="horiz_separator" />';
                    }
                    $output.='</li>';
                }
            }
        }

        $i++;
    }

    return $output;
}

function user_menu_generate_level1_children($tree)
{
    global $vmagmenu;
    global $vmenuh;
    global $smenuh;
    $count=count($tree);
    $i =1;
    $output='';

    if($smenuh)
    {
        $output.='<ul id="dropdown-menu" class="child dropdown-menu" role="menu">';
    }
    else
    {
        $output.='<ul id="dropdown-menu" class="child collapse" role="menu">';
    }

    foreach($tree as $tree_item)
    {
        if ($tree_item['link']['hidden'] == 0)
        {
            if($tree_item['link']['has_children'])
            {

                if($vmagmenu)
                {
                    $output.= '<li class="span1 unstyled dropdown dropdown-submenu">';
                    $output.='<a class="subchild dropdown-toggle" data-toggle="dropdown" href="' . check_url(url($tree_item['link']['href'], $tree_item['link']['localized_options'])) . '"><span class="menuchildicon"></span>' .  $tree_item['link']['link_title'] . '</a>';
                    if ($vmagmenu)
                    {
                        $output.='<hr class="separator" />';
                    }
                    else
                    {
                        if($i != $count)
                        {
                            $output.='<hr class="separator" />';
                        }
                    }

                }

                else
                {

                    $output.='<li class ="dropdown dropdown-submenu"> <a class="subchild dropdown-toggle" data-toggle="dropdown" href="' . check_url(url($tree_item['link']['href'], $tree_item['link']['localized_options'])) . '"><span class="menuchildicon"></span>' .  $tree_item['link']['link_title'] . '</a>';
                    if ($vmagmenu)
                    {
                        $output.='<hr class="separator" />';
                    }
                    else
                    {
                        if($i != $count)
                        {
                            $output.='<hr class="separator" />';
                        }
                    }

                }

                if($vmagmenu)
                {
                    $output.= user_menu_generate_level2_children($tree_item['below']);
                    $output.= '</li>';

                }
                else
                {
                    $output.= user_menu_generate_level2_children($tree_item['below']);
                    $output.= '</li>';
                }

            }
            else
            {
                $output.= '<li><a href="' . check_url(url($tree_item['link']['href'], $tree_item['link']['localized_options'])).  '"><span class="menuchildicon"></span>' .   $tree_item['link']['link_title'] .  '</a>';

                if ($vmagmenu)
                {
                    $output.='<hr class="separator" />';
                }
                else
                {
                    if($i != $count)
                    {
                        $output.='<hr class="separator" />';
                    }

                }
                $output.='</li>';

            }
        }
        $i++;
    }

    $output.='</ul>';
    return $output;
}

function user_menu_generate_level2_children($tree)
{
    global $vmagmenu;
    global $vmenuh;
    global $smenuh;
    $count=count($tree);
    $i =1;
    $output='';

    if($vmagmenu)
    {
        $output.='<ul  role="menu">';
    }
    else
    {
        if($vmenuh)
        {
            $output.='<ul class="sub-menu" role="menu">';
        }

        else
        {
            $output.='<ul id="dropdown-menu" class="dropdown-menu sub-menu" role="menu">';
        }
    }

    foreach($tree as $tree_item)
    {
        if ($tree_item['link']['hidden'] == 0)
        {

            if($tree_item['link']['has_children'])
            {
                $output.='<li class="dropdown dropdown-submenu"><a class="subchild dropdown-toggle" data-toggle="dropdown" href="' . check_url(url($tree_item['link']['href'], $tree_item['link']['localized_options'])) . '"><span class="menuchildicon"></span>' . $tree_item['link']['link_title'] . '</a>';
                if ($vmagmenu)
                {
                    $output.='<hr class="separator" />';
                }
                else
                {
                    if($i != $count)
                    {
                        $output.='<hr class="separator" />';
                    }

                }

                $output.=user_menu_generate_level2_children($tree_item['below']);
                $output.='</li>';
            }
            else{
                $output.='<li><a href="' . check_url(url($tree_item['link']['href'], $tree_item['link']['localized_options'])) . '"><span class="menuchildicon"></span>' . $tree_item['link']['link_title'] . '</a>';
                if ($vmagmenu)
                {
                    $output.='<hr class="separator" />';
                }
                else
                {
                    if($i != $count)
                    {
                        $output.='<hr class="separator" />';
                    }

                }
                $output.='</li>';
            }
        }
        $i++;
    }
    $output.='</ul>';
    return $output;
}
*/
?>