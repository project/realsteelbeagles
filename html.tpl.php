<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php print $head; ?>
<title><?php print $head_title; ?></title>
<?php print $styles; ?>
<?php print $scripts; ?>
<?php $theme_path = base_path() . path_to_theme(); ?>
<script type="text/javascript" src="<?php echo $theme_path?>/totop.js">
</script>
<script type="text/javascript" src="<?php echo $theme_path?>/tt_animation.js">
</script>
<script type="text/javascript" src="<?php echo $theme_path?>/tt_slideshow.js">
</script>
<!--[if lt IE 9]>
<script type="text/javascript" src="<?php echo $theme_path?>/html5shiv.js">
</script>
<script type="text/javascript" src="<?php echo $theme_path?>/respond.min.js">
</script>
<![endif]-->
</head>
<body class="<?php print $classes; ?>"<?php print $attributes;?>>
<div class="totopshow">
<a href="#" class="back-to-top"><img alt="Back to Top" src="<?php echo $theme_path?>/images/gototop.png"/></a>
</div>
<div id="skip-link">
<a class="element-invisible element-focusable" href="#main-content"><?php print t('Skip to main content'); ?></a>
 </div>
<?php print $page_top; ?>
<?php print $page; ?>
<?php print $page_bottom; ?>
</body>
</html>
