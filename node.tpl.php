<?php global $columncounter;
global $featurepost;
if (drupal_is_front_page()) {
if ($featurepost) {
$lg = 1;
$md = 1;
$xs = 1;
} else {
$lg = 4;
$md = 4;
$xs = 1;
}
$class_suffix_lg = round((12 / $lg));
$class_suffix_md = round((12 / $md));
$class_suffix_xs = round((12 / $xs)); ?>
<div class="col-xl-<?php echo $class_suffix_lg; ?> col-lg-<?php echo $class_suffix_lg; ?> col-md-<?php echo $class_suffix_md; ?> col-sm-<?php echo $class_suffix_xs; ?> col-xs-<?php echo
$class_suffix_xs; ?> col-<?php echo
$class_suffix_xs; ?>">
<?php } ?>
<article id="node-<?php print $node->nid; ?>" class="ttr_post <?php print $classes; ?>">
<div class="ttr_post_content_inner">
<?php if (!drupal_is_front_page()) { ?>
	<div class="ttr_post_inner_box">
	<?php print render($title_prefix); ?>
	<h2 class="ttr_post_title">
	<?php $theme_path = base_path() . path_to_theme(); ?>
	<a href="<?php print $node_url; ?>">
	<?php print $title; ?>
	</a>
	</h2>
	<?php print render($title_suffix); ?>
	</div>
<?php } ?>	
<div class="ttr_article">
<?php if ($display_submitted): ?>
<?php endif; ?>
<div  class="postcontent"<?php print $content_attributes; ?>>
<?php hide($content['comments']); ?>
<?php hide($content['links']); ?>
<?php print render($content);
if ($featurepost) {
$columncounter = 0;
} else {
$columncounter++;
} ?>
<div style="clear:both;"></div>
<?php if (!empty($content['links'])): ?>
<div class="links"><?php print render($content['links']); ?></div>
<?php endif; ?>
<?php print render($content['comments']); ?>
</div>
</div>
</div>
</article>
<?php if (drupal_is_front_page()) {
echo "</div>";
if ($featurepost) {
echo '<div class= "visible-sm-block visible-md-block visible-lg-block visible-xs-block d-xl-block d-lg-block d-sm-block d-md-block d-block" style="clear:both;"></div>';
$featurepost = false;
} else {
if (($columncounter) % $xs == 0) {
echo '<div class="visible-sm-block d-sm-block visible-xs-block d-block" style="clear:both;"></div>';
}
if (($columncounter) % $md == 0) {
echo '<div class="visible-md-block d-md-block" style="clear:both;"></div>';
}
if (($columncounter) % $lg == 0) {
echo '<div class="visible-lg-block d-xl-block d-lg-block" style="clear:both;"></div>';
}
}
} ?>
