<?php
 /**
 * @file
 * Theme setting callbacks for therealsteelbeagles theme
*/ 
 
 /**
 * Implements hook_form_FORM_ID_alter().
 * @param $form
 * The form.
 * @param $form_state
 * The form state.
*/ 

 
function realsteelbeagles_form_system_theme_settings_alter(&$form, &$form_state) { 
if (module_exists('jquery_update')){ 
 $jquery_version = variable_get('jquery_update_jquery_version');
if (!version_compare($jquery_version, '1.9', '>=')){ 
 $message = t('The theme requires a minimum jQuery version of 1.9 . Please set your jquery version to 1.9 or higher from <a href="!jquery_update_configure">Jquery_Update configuration</a>.', array( 
'!jquery_update_configure' => check_plain(url('/admin/config/development/jquery_update')) )); 
  drupal_set_message($message, 'error');
} 
} 
else{ 
$message = t('jQuery_Update module is not installed, Theme requires a minimum jQuery version of 1.9 or higher. Please install the <a href="!jquery_update_project_url">jQuery Update</a> module.', array( 
 '!jquery_update_project_url' => check_plain('https://www.drupal.org/project/jquery_update') )); 
  drupal_set_message($message, 'error');
 }
$form[' ttr_settings'] = array(
	'#type' => 'fieldset',
	'#title' => t(' Theme Settings'),
	'#collapsible' => FALSE,
	'#collapsed' => FALSE,
);


$form[' ttr_settings']['tabs'] = array(
	'#type' => 'vertical_tabs',
);


$form[' ttr_settings']['tabs']['menu_style_options'] = array(
	'#type' => 'fieldset',
	'#title' => t('Menu style options'),
	'#collapsible' => TRUE,
	'#collapsed' => TRUE,
);


 $form[' ttr_settings']['tabs']['menu_style_options']['Menu'] = array(
	'#type' => 'fieldset',
	'#title' => t('Select menu style'),
);
$form[' ttr_settings']['tabs']['menu_style_options']['Menu']['menu_headerabovecolumn1'] = array(
	'#type' => 'select',
	'#title' => t('Headerabovecolumn1'),
	'#options' => array(
	 0 => t('Horizontal'),
	 1 => t('Vertical'),
	),
	 '#default_value' => theme_get_setting('menu_headerabovecolumn1'),
 );
$form[' ttr_settings']['tabs']['menu_style_options']['Menu']['menu_headerabovecolumn2'] = array(
	'#type' => 'select',
	'#title' => t('Headerabovecolumn2'),
	'#options' => array(
	 0 => t('Horizontal'),
	 1 => t('Vertical'),
	),
	 '#default_value' => theme_get_setting('menu_headerabovecolumn2'),
 );
$form[' ttr_settings']['tabs']['menu_style_options']['Menu']['menu_headerabovecolumn3'] = array(
	'#type' => 'select',
	'#title' => t('Headerabovecolumn3'),
	'#options' => array(
	 0 => t('Horizontal'),
	 1 => t('Vertical'),
	),
	 '#default_value' => theme_get_setting('menu_headerabovecolumn3'),
 );
$form[' ttr_settings']['tabs']['menu_style_options']['Menu']['menu_headerabovecolumn4'] = array(
	'#type' => 'select',
	'#title' => t('Headerabovecolumn4'),
	'#options' => array(
	 0 => t('Horizontal'),
	 1 => t('Vertical'),
	),
	 '#default_value' => theme_get_setting('menu_headerabovecolumn4'),
 );
$form[' ttr_settings']['tabs']['menu_style_options']['Menu']['menu_headerbelowcolumn1'] = array(
	'#type' => 'select',
	'#title' => t('Headerbelowcolumn1'),
	'#options' => array(
	 0 => t('Horizontal'),
	 1 => t('Vertical'),
	),
	 '#default_value' => theme_get_setting('menu_headerbelowcolumn1'),
 );
$form[' ttr_settings']['tabs']['menu_style_options']['Menu']['menu_headerbelowcolumn2'] = array(
	'#type' => 'select',
	'#title' => t('Headerbelowcolumn2'),
	'#options' => array(
	 0 => t('Horizontal'),
	 1 => t('Vertical'),
	),
	 '#default_value' => theme_get_setting('menu_headerbelowcolumn2'),
 );
$form[' ttr_settings']['tabs']['menu_style_options']['Menu']['menu_headerbelowcolumn3'] = array(
	'#type' => 'select',
	'#title' => t('Headerbelowcolumn3'),
	'#options' => array(
	 0 => t('Horizontal'),
	 1 => t('Vertical'),
	),
	 '#default_value' => theme_get_setting('menu_headerbelowcolumn3'),
 );
$form[' ttr_settings']['tabs']['menu_style_options']['Menu']['menu_headerbelowcolumn4'] = array(
	'#type' => 'select',
	'#title' => t('Headerbelowcolumn4'),
	'#options' => array(
	 0 => t('Horizontal'),
	 1 => t('Vertical'),
	),
	 '#default_value' => theme_get_setting('menu_headerbelowcolumn4'),
 );
$form[' ttr_settings']['tabs']['menu_style_options']['Menu']['menu_slideshowabovecolumn1'] = array(
	'#type' => 'select',
	'#title' => t('Slideshowabovecolumn1'),
	'#options' => array(
	 0 => t('Horizontal'),
	 1 => t('Vertical'),
	),
	 '#default_value' => theme_get_setting('menu_slideshowabovecolumn1'),
 );
$form[' ttr_settings']['tabs']['menu_style_options']['Menu']['menu_slideshowabovecolumn2'] = array(
	'#type' => 'select',
	'#title' => t('Slideshowabovecolumn2'),
	'#options' => array(
	 0 => t('Horizontal'),
	 1 => t('Vertical'),
	),
	 '#default_value' => theme_get_setting('menu_slideshowabovecolumn2'),
 );
$form[' ttr_settings']['tabs']['menu_style_options']['Menu']['menu_slideshowabovecolumn3'] = array(
	'#type' => 'select',
	'#title' => t('Slideshowabovecolumn3'),
	'#options' => array(
	 0 => t('Horizontal'),
	 1 => t('Vertical'),
	),
	 '#default_value' => theme_get_setting('menu_slideshowabovecolumn3'),
 );
$form[' ttr_settings']['tabs']['menu_style_options']['Menu']['menu_slideshowabovecolumn4'] = array(
	'#type' => 'select',
	'#title' => t('Slideshowabovecolumn4'),
	'#options' => array(
	 0 => t('Horizontal'),
	 1 => t('Vertical'),
	),
	 '#default_value' => theme_get_setting('menu_slideshowabovecolumn4'),
 );
$form[' ttr_settings']['tabs']['menu_style_options']['Menu']['menu_slideshowbelowcolumn1'] = array(
	'#type' => 'select',
	'#title' => t('Slideshowbelowcolumn1'),
	'#options' => array(
	0 => t('Horizontal'),
	1 => t('Vertical'),
	),
	 '#default_value' => theme_get_setting('menu_slideshowbelowcolumn1'),
 );
$form[' ttr_settings']['tabs']['menu_style_options']['Menu']['menu_slideshowbelowcolumn2'] = array(
	'#type' => 'select',
	'#title' => t('Slideshowbelowcolumn2'),
	'#options' => array(
	0 => t('Horizontal'),
	1 => t('Vertical'),
	),
	 '#default_value' => theme_get_setting('menu_slideshowbelowcolumn2'),
 );
$form[' ttr_settings']['tabs']['menu_style_options']['Menu']['menu_slideshowbelowcolumn3'] = array(
	'#type' => 'select',
	'#title' => t('Slideshowbelowcolumn3'),
	'#options' => array(
	0 => t('Horizontal'),
	1 => t('Vertical'),
	),
	 '#default_value' => theme_get_setting('menu_slideshowbelowcolumn3'),
 );
$form[' ttr_settings']['tabs']['menu_style_options']['Menu']['menu_slideshowbelowcolumn4'] = array(
	'#type' => 'select',
	'#title' => t('Slideshowbelowcolumn4'),
	'#options' => array(
	0 => t('Horizontal'),
	1 => t('Vertical'),
	),
	 '#default_value' => theme_get_setting('menu_slideshowbelowcolumn4'),
 );
$form[' ttr_settings']['tabs']['menu_style_options']['Menu']['menu_menuabovecolumn1'] = array(
	'#type' => 'select',
	'#title' => t('menuabovecolumn1'),
	'#options' => array(
	0 => t('Horizontal'),
	1 => t('Vertical'),
	),
	 '#default_value' => theme_get_setting('menu_menuabovecolumn1'),
 );
$form[' ttr_settings']['tabs']['menu_style_options']['Menu']['menu_menuabovecolumn2'] = array(
	'#type' => 'select',
	'#title' => t('menuabovecolumn2'),
	'#options' => array(
	0 => t('Horizontal'),
	1 => t('Vertical'),
	),
	 '#default_value' => theme_get_setting('menu_menuabovecolumn2'),
 );
$form[' ttr_settings']['tabs']['menu_style_options']['Menu']['menu_menuabovecolumn3'] = array(
	'#type' => 'select',
	'#title' => t('menuabovecolumn3'),
	'#options' => array(
	0 => t('Horizontal'),
	1 => t('Vertical'),
	),
	 '#default_value' => theme_get_setting('menu_menuabovecolumn3'),
 );
$form[' ttr_settings']['tabs']['menu_style_options']['Menu']['menu_menuabovecolumn4'] = array(
	'#type' => 'select',
	'#title' => t('menuabovecolumn4'),
	'#options' => array(
	0 => t('Horizontal'),
	1 => t('Vertical'),
	),
	 '#default_value' => theme_get_setting('menu_menuabovecolumn4'),
 );
$form[' ttr_settings']['tabs']['menu_style_options']['Menu']['menu_menubelowcolumn1'] = array(
	'#type' => 'select',
	'#title' => t('Menubelowcolumn1'),
	'#options' => array(
	0 => t('Horizontal'),
	1 => t('Vertical'),
	),
	 '#default_value' => theme_get_setting('menu_menubelowcolumn1'),
 );
$form[' ttr_settings']['tabs']['menu_style_options']['Menu']['menu_menubelowcolumn2'] = array(
	'#type' => 'select',
	'#title' => t('Menubelowcolumn2'),
	'#options' => array(
	0 => t('Horizontal'),
	1 => t('Vertical'),
	),
	 '#default_value' => theme_get_setting('menu_menubelowcolumn2'),
 );
$form[' ttr_settings']['tabs']['menu_style_options']['Menu']['menu_menubelowcolumn3'] = array(
	'#type' => 'select',
	'#title' => t('Menubelowcolumn3'),
	'#options' => array(
	0 => t('Horizontal'),
	1 => t('Vertical'),
	),
	 '#default_value' => theme_get_setting('menu_menubelowcolumn3'),
 );
$form[' ttr_settings']['tabs']['menu_style_options']['Menu']['menu_menubelowcolumn4'] = array(
	'#type' => 'select',
	'#title' => t('Menubelowcolumn4'),
	'#options' => array(
	0 => t('Horizontal'),
	1 => t('Vertical'),
	),
	 '#default_value' => theme_get_setting('menu_menubelowcolumn4'),
 );
$form[' ttr_settings']['tabs']['menu_style_options']['Menu']['menu_contenttopcolumn1'] = array(
	'#type' => 'select',
	'#title' => t('Contenttopcolumn1'),
	'#options' => array(
	0 => t('Horizontal'),
	1 => t('Vertical'),
	),
	 '#default_value' => theme_get_setting('menu_contenttopcolumn1'),
 );
$form[' ttr_settings']['tabs']['menu_style_options']['Menu']['menu_contenttopcolumn2'] = array(
	'#type' => 'select',
	'#title' => t('Contenttopcolumn2'),
	'#options' => array(
	0 => t('Horizontal'),
	1 => t('Vertical'),
	),
	 '#default_value' => theme_get_setting('menu_contenttopcolumn2'),
 );
$form[' ttr_settings']['tabs']['menu_style_options']['Menu']['menu_contenttopcolumn3'] = array(
	'#type' => 'select',
	'#title' => t('Contenttopcolumn3'),
	'#options' => array(
	0 => t('Horizontal'),
	1 => t('Vertical'),
	),
	 '#default_value' => theme_get_setting('menu_contenttopcolumn3'),
 );
$form[' ttr_settings']['tabs']['menu_style_options']['Menu']['menu_contenttopcolumn4'] = array(
	'#type' => 'select',
	'#title' => t('Contenttopcolumn4'),
	'#options' => array(
	0 => t('Horizontal'),
	1 => t('Vertical'),
	),
	 '#default_value' => theme_get_setting('menu_contenttopcolumn4'),
 );
$form[' ttr_settings']['tabs']['menu_style_options']['Menu']['menu_contentbottomcolumn1'] = array(
	'#type' => 'select',
	'#title' => t('Contentbottomcolumn1'),
	'#options' => array(
	0 => t('Horizontal'),
	1 => t('Vertical'),
	),
	 '#default_value' => theme_get_setting('menu_contentbottomcolumn1'),
 );
$form[' ttr_settings']['tabs']['menu_style_options']['Menu']['menu_contentbottomcolumn2'] = array(
	'#type' => 'select',
	'#title' => t('Contentbottomcolumn2'),
	'#options' => array(
	0 => t('Horizontal'),
	1 => t('Vertical'),
	),
	 '#default_value' => theme_get_setting('menu_contentbottomcolumn2'),
 );
$form[' ttr_settings']['tabs']['menu_style_options']['Menu']['menu_contentbottomcolumn3'] = array(
	'#type' => 'select',
	'#title' => t('Contentbottomcolumn3'),
	'#options' => array(
	0 => t('Horizontal'),
	1 => t('Vertical'),
	),
	 '#default_value' => theme_get_setting('menu_contentbottomcolumn3'),
 );
$form[' ttr_settings']['tabs']['menu_style_options']['Menu']['menu_contentbottomcolumn4'] = array(
	'#type' => 'select',
	'#title' => t('Contentbottomcolumn4'),
	'#options' => array(
	0 => t('Horizontal'),
	1 => t('Vertical'),
	),
	 '#default_value' => theme_get_setting('menu_contentbottomcolumn4'),
 );
$form[' ttr_settings']['tabs']['menu_style_options']['Menu']['menu_footerabovecolumn1'] = array(
	'#type' => 'select',
	'#title' => t('Footerabovecolumn1'),
	'#options' => array(
	0 => t('Horizontal'),
	1 => t('Vertical'),
	),
	 '#default_value' => theme_get_setting('menu_footerabovecolumn1'),
 );
$form[' ttr_settings']['tabs']['menu_style_options']['Menu']['menu_footerabovecolumn2'] = array(
	'#type' => 'select',
	'#title' => t('Footerabovecolumn2'),
	'#options' => array(
	0 => t('Horizontal'),
	1 => t('Vertical'),
	),
	 '#default_value' => theme_get_setting('menu_footerabovecolumn2'),
 );
$form[' ttr_settings']['tabs']['menu_style_options']['Menu']['menu_footerabovecolumn3'] = array(
	'#type' => 'select',
	'#title' => t('Footerabovecolumn3'),
	'#options' => array(
	0 => t('Horizontal'),
	1 => t('Vertical'),
	),
	 '#default_value' => theme_get_setting('menu_footerabovecolumn3'),
 );
$form[' ttr_settings']['tabs']['menu_style_options']['Menu']['menu_footerabovecolumn4'] = array(
	'#type' => 'select',
	'#title' => t('Footerabovecolumn4'),
	'#options' => array(
	0 => t('Horizontal'),
	1 => t('Vertical'),
	),
	 '#default_value' => theme_get_setting('menu_footerabovecolumn4'),
 );
$form[' ttr_settings']['tabs']['menu_style_options']['Menu']['menu_footerbelowcolumn1'] = array(
	'#type' => 'select',
	'#title' => t('Footerbelowcolumn1'),
	'#options' => array(
	0 => t('Horizontal'),
	1 => t('Vertical'),
	),
	 '#default_value' => theme_get_setting('menu_footerbelowcolumn1'),
 );
$form[' ttr_settings']['tabs']['menu_style_options']['Menu']['menu_footerbelowcolumn2'] = array(
	'#type' => 'select',
	'#title' => t('Footerbelowcolumn2'),
	'#options' => array(
	0 => t('Horizontal'),
	1 => t('Vertical'),
	),
	 '#default_value' => theme_get_setting('menu_footerbelowcolumn2'),
 );
$form[' ttr_settings']['tabs']['menu_style_options']['Menu']['menu_footerbelowcolumn3'] = array(
	'#type' => 'select',
	'#title' => t('Footerbelowcolumn3'),
	'#options' => array(
	0 => t('Horizontal'),
	1 => t('Vertical'),
	),
	 '#default_value' => theme_get_setting('menu_footerbelowcolumn3'),
 );
$form[' ttr_settings']['tabs']['menu_style_options']['Menu']['menu_footerbelowcolumn4'] = array(
	'#type' => 'select',
	'#title' => t('Footerbelowcolumn4'),
	'#options' => array(
	0 => t('Horizontal'),
	1 => t('Vertical'),
	),
	 '#default_value' => theme_get_setting('menu_footerbelowcolumn4'),
 );
$form[' ttr_settings']['tabs']['menu_style_options']['Menu']['menu_leftfooterarea'] = array(
	'#type' => 'select',
	'#title' => t('Leftfooterarea'),
	'#options' => array(
	0 => t('Horizontal'),
	1 => t('Vertical'),
	),
	 '#default_value' => theme_get_setting('menu_leftfooterarea'),
 );
$form[' ttr_settings']['tabs']['menu_style_options']['Menu']['menu_centerfooterarea'] = array(
	'#type' => 'select',
	'#title' => t('Centerfooterarea'),
	'#options' => array(
	0 => t('Horizontal'),
	1 => t('Vertical'),
	),
	 '#default_value' => theme_get_setting('menu_centerfooterarea'),
 );
$form[' ttr_settings']['tabs']['menu_style_options']['Menu']['menu_rightfooterarea'] = array(
	'#type' => 'select',
	'#title' => t('Rightfooterarea'),
	'#options' => array(
	0 => t('Horizontal'),
	1 => t('Vertical'),
	),
	 '#default_value' => theme_get_setting('menu_rightfooterarea'),
 );


 $form[' ttr_settings']['tabs']['style_options'] = array(
	'#type' => 'fieldset',
	'#title' => t('Style options'),
);


 $form[' ttr_settings']['tabs']['style_options']['style'] = array(
	'#type' => 'fieldset',
	'#title' => t('Select Position Style '),
);
$form[' ttr_settings']['tabs']['style_options']['style']['style_headerabovecolumn1'] = array(
	'#type' => 'select',
	'#title' => t('Headerabovecolumn1'),
	'#options' => array(
	 0 => t('TTDefault'),
	 1 => t('None'),
	),
	 '#default_value' => theme_get_setting('style_headerabovecolumn1'),
 );
$form[' ttr_settings']['tabs']['style_options']['style']['style_headerabovecolumn2'] = array(
	'#type' => 'select',
	'#title' => t('Headerabovecolumn2'),
	'#options' => array(
	 0 => t('TTDefault'),
	 1 => t('None'),
	),
	 '#default_value' => theme_get_setting('style_headerabovecolumn2'),
 );
$form[' ttr_settings']['tabs']['style_options']['style']['style_headerabovecolumn3'] = array(
	'#type' => 'select',
	'#title' => t('Headerabovecolumn3'),
	'#options' => array(
	 0 => t('TTDefault'),
	 1 => t('None'),
	),
	 '#default_value' => theme_get_setting('style_headerabovecolumn3'),
 );
$form[' ttr_settings']['tabs']['style_options']['style']['style_headerabovecolumn4'] = array(
	'#type' => 'select',
	'#title' => t('Headerabovecolumn4'),
	'#options' => array(
	 0 => t('TTDefault'),
	 1 => t('None'),
	),
	 '#default_value' => theme_get_setting('style_headerabovecolumn4'),
 );
$form[' ttr_settings']['tabs']['style_options']['style']['style_headerbelowcolumn1'] = array(
	'#type' => 'select',
	'#title' => t('Headerbelowcolumn1'),
	'#options' => array(
	 0 => t('TTDefault'),
	 1 => t('None'),
	),
	 '#default_value' => theme_get_setting('style_headerbelowcolumn1'),
 );
$form[' ttr_settings']['tabs']['style_options']['style']['style_headerbelowcolumn2'] = array(
	'#type' => 'select',
	'#title' => t('Headerbelowcolumn2'),
	'#options' => array(
	 0 => t('TTDefault'),
	 1 => t('None'),
	),
	 '#default_value' => theme_get_setting('style_headerbelowcolumn2'),
 );
$form[' ttr_settings']['tabs']['style_options']['style']['style_headerbelowcolumn3'] = array(
	'#type' => 'select',
	'#title' => t('Headerbelowcolumn3'),
	'#options' => array(
	 0 => t('TTDefault'),
	 1 => t('None'),
	),
	 '#default_value' => theme_get_setting('style_headerbelowcolumn3'),
 );
$form[' ttr_settings']['tabs']['style_options']['style']['style_headerbelowcolumn4'] = array(
	'#type' => 'select',
	'#title' => t('Headerbelowcolumn4'),
	'#options' => array(
	 0 => t('TTDefault'),
	 1 => t('None'),
	),
	 '#default_value' => theme_get_setting('style_headerbelowcolumn4'),
 );
$form[' ttr_settings']['tabs']['style_options']['style']['style_slideshowabovecolumn1'] = array(
	'#type' => 'select',
	'#title' => t('Slideshowabovecolumn1'),
	'#options' => array(
	 0 => t('TTDefault'),
	 1 => t('None'),
	),
	 '#default_value' => theme_get_setting('style_slideshowabovecolumn1'),
 );
$form[' ttr_settings']['tabs']['style_options']['style']['style_slideshowabovecolumn2'] = array(
	'#type' => 'select',
	'#title' => t('Slideshowabovecolumn2'),
	'#options' => array(
	 0 => t('TTDefault'),
	 1 => t('None'),
	),
	 '#default_value' => theme_get_setting('style_slideshowabovecolumn2'),
 );
$form[' ttr_settings']['tabs']['style_options']['style']['style_slideshowabovecolumn3'] = array(
	'#type' => 'select',
	'#title' => t('Slideshowabovecolumn3'),
	'#options' => array(
	 0 => t('TTDefault'),
	 1 => t('None'),
	),
	 '#default_value' => theme_get_setting('style_slideshowabovecolumn3'),
 );
$form[' ttr_settings']['tabs']['style_options']['style']['style_slideshowabovecolumn4'] = array(
	'#type' => 'select',
	'#title' => t('Slideshowabovecolumn4'),
	'#options' => array(
	 0 => t('TTDefault'),
	 1 => t('None'),
	),
	 '#default_value' => theme_get_setting('style_slideshowabovecolumn4'),
 );
$form[' ttr_settings']['tabs']['style_options']['style']['style_slideshowbelowcolumn1'] = array(
	'#type' => 'select',
	'#title' => t('Slideshowbelowcolumn1'),
	'#options' => array(
	0 => t('TTDefault'),
	1 => t('None'),
	),
	 '#default_value' => theme_get_setting('style_slideshowbelowcolumn1'),
 );
$form[' ttr_settings']['tabs']['style_options']['style']['style_slideshowbelowcolumn2'] = array(
	'#type' => 'select',
	'#title' => t('Slideshowbelowcolumn2'),
	'#options' => array(
	0 => t('TTDefault'),
	1 => t('None'),
	),
	 '#default_value' => theme_get_setting('style_slideshowbelowcolumn2'),
 );
$form[' ttr_settings']['tabs']['style_options']['style']['style_slideshowbelowcolumn3'] = array(
	'#type' => 'select',
	'#title' => t('Slideshowbelowcolumn3'),
	'#options' => array(
	0 => t('TTDefault'),
	1 => t('None'),
	),
	 '#default_value' => theme_get_setting('style_slideshowbelowcolumn3'),
 );
$form[' ttr_settings']['tabs']['style_options']['style']['style_slideshowbelowcolumn4'] = array(
	'#type' => 'select',
	'#title' => t('Slideshowbelowcolumn4'),
	'#options' => array(
	0 => t('TTDefault'),
	1 => t('None'),
	),
	 '#default_value' => theme_get_setting('style_slideshowbelowcolumn4'),
 );
$form[' ttr_settings']['tabs']['style_options']['style']['style_menuabovecolumn1'] = array(
	'#type' => 'select',
	'#title' => t('Menuabovecolumn1'),
	'#options' => array(
	0 => t('TTDefault'),
	1 => t('None'),
	),
	 '#default_value' => theme_get_setting('style_menuabovecolumn1'),
 );
$form[' ttr_settings']['tabs']['style_options']['style']['style_menuabovecolumn2'] = array(
	'#type' => 'select',
	'#title' => t('Menuabovecolumn2'),
	'#options' => array(
	0 => t('TTDefault'),
	1 => t('None'),
	),
	 '#default_value' => theme_get_setting('style_menuabovecolumn2'),
 );
$form[' ttr_settings']['tabs']['style_options']['style']['style_menuabovecolumn3'] = array(
	'#type' => 'select',
	'#title' => t('Menuabovecolumn3'),
	'#options' => array(
	0 => t('TTDefault'),
	1 => t('None'),
	),
	 '#default_value' => theme_get_setting('style_menuabovecolumn3'),
 );
$form[' ttr_settings']['tabs']['style_options']['style']['style_menuabovecolumn4'] = array(
	'#type' => 'select',
	'#title' => t('Menuabovecolumn4'),
	'#options' => array(
	0 => t('TTDefault'),
	1 => t('None'),
	),
	 '#default_value' => theme_get_setting('style_menuabovecolumn4'),
 );
$form[' ttr_settings']['tabs']['style_options']['style']['style_menubelowcolumn1'] = array(
	'#type' => 'select',
	'#title' => t('Menubelowcolumn1'),
	'#options' => array(
	0 => t('TTDefault'),
	1 => t('None'),
	),
	 '#default_value' => theme_get_setting('style_menubelowcolumn1'),
 );
$form[' ttr_settings']['tabs']['style_options']['style']['style_menubelowcolumn2'] = array(
	'#type' => 'select',
	'#title' => t('Menubelowcolumn2'),
	'#options' => array(
	0 => t('TTDefault'),
	1 => t('None'),
	),
	 '#default_value' => theme_get_setting('style_menubelowcolumn2'),
 );
$form[' ttr_settings']['tabs']['style_options']['style']['style_menubelowcolumn3'] = array(
	'#type' => 'select',
	'#title' => t('Menubelowcolumn3'),
	'#options' => array(
	0 => t('TTDefault'),
	1 => t('None'),
	),
	 '#default_value' => theme_get_setting('style_menubelowcolumn3'),
 );
$form[' ttr_settings']['tabs']['style_options']['style']['style_menubelowcolumn4'] = array(
	'#type' => 'select',
	'#title' => t('Menubelowcolumn4'),
	'#options' => array(
	0 => t('TTDefault'),
	1 => t('None'),
	),
	 '#default_value' => theme_get_setting('style_menubelowcolumn4'),
 );
$form[' ttr_settings']['tabs']['style_options']['style']['style_contenttopcolumn1'] = array(
	'#type' => 'select',
	'#title' => t('Contenttopcolumn1'),
	'#options' => array(
	0 => t('TTDefault'),
	1 => t('None'),
	),
	 '#default_value' => theme_get_setting('style_contenttopcolumn1'),
 );
$form[' ttr_settings']['tabs']['style_options']['style']['style_contenttopcolumn2'] = array(
	'#type' => 'select',
	'#title' => t('Contenttopcolumn2'),
	'#options' => array(
	0 => t('TTDefault'),
	1 => t('None'),
	),
	 '#default_value' => theme_get_setting('style_contenttopcolumn2'),
 );
$form[' ttr_settings']['tabs']['style_options']['style']['style_contenttopcolumn3'] = array(
	'#type' => 'select',
	'#title' => t('Contenttopcolumn3'),
	'#options' => array(
	0 => t('TTDefault'),
	1 => t('None'),
	),
	 '#default_value' => theme_get_setting('style_contenttopcolumn3'),
 );
$form[' ttr_settings']['tabs']['style_options']['style']['style_contenttopcolumn4'] = array(
	'#type' => 'select',
	'#title' => t('Contenttopcolumn4'),
	'#options' => array(
	0 => t('TTDefault'),
	1 => t('None'),
	),
	 '#default_value' => theme_get_setting('style_contenttopcolumn4'),
 );
$form[' ttr_settings']['tabs']['style_options']['style']['style_contentbottomcolumn1'] = array(
	'#type' => 'select',
	'#title' => t('Contentbottomcolumn1'),
	'#options' => array(
	0 => t('TTDefault'),
	1 => t('None'),
	),
	 '#default_value' => theme_get_setting('style_contentbottomcolumn1'),
 );
$form[' ttr_settings']['tabs']['style_options']['style']['style_contentbottomcolumn2'] = array(
	'#type' => 'select',
	'#title' => t('Contentbottomcolumn2'),
	'#options' => array(
	0 => t('TTDefault'),
	1 => t('None'),
	),
	 '#default_value' => theme_get_setting('style_contentbottomcolumn2'),
 );
$form[' ttr_settings']['tabs']['style_options']['style']['style_contentbottomcolumn3'] = array(
	'#type' => 'select',
	'#title' => t('Contentbottomcolumn3'),
	'#options' => array(
	0 => t('TTDefault'),
	1 => t('None'),
	),
	 '#default_value' => theme_get_setting('style_contentbottomcolumn3'),
 );
$form[' ttr_settings']['tabs']['style_options']['style']['style_contentbottomcolumn4'] = array(
	'#type' => 'select',
	'#title' => t('Contentbottomcolumn4'),
	'#options' => array(
	0 => t('TTDefault'),
	1 => t('None'),
	),
	 '#default_value' => theme_get_setting('style_contentbottomcolumn4'),
 );
$form[' ttr_settings']['tabs']['style_options']['style']['style_footerabovecolumn1'] = array(
	'#type' => 'select',
	'#title' => t('Footerabovecolumn1'),
	'#options' => array(
	0 => t('TTDefault'),
	1 => t('None'),
	),
	 '#default_value' => theme_get_setting('style_footerabovecolumn1'),
 );
$form[' ttr_settings']['tabs']['style_options']['style']['style_footerabovecolumn2'] = array(
	'#type' => 'select',
	'#title' => t('Footerabovecolumn2'),
	'#options' => array(
	0 => t('TTDefault'),
	1 => t('None'),
	),
	 '#default_value' => theme_get_setting('style_footerabovecolumn2'),
 );
$form[' ttr_settings']['tabs']['style_options']['style']['style_footerabovecolumn3'] = array(
	'#type' => 'select',
	'#title' => t('Footerabovecolumn3'),
	'#options' => array(
	0 => t('TTDefault'),
	1 => t('None'),
	),
	 '#default_value' => theme_get_setting('style_footerabovecolumn3'),
 );
$form[' ttr_settings']['tabs']['style_options']['style']['style_footerabovecolumn4'] = array(
	'#type' => 'select',
	'#title' => t('Footerabovecolumn4'),
	'#options' => array(
	0 => t('TTDefault'),
	1 => t('None'),
	),
	 '#default_value' => theme_get_setting('style_footerabovecolumn4'),
 );
$form[' ttr_settings']['tabs']['style_options']['style']['style_footerbelowcolumn1'] = array(
	'#type' => 'select',
	'#title' => t('Footerbelowcolumn1'),
	'#options' => array(
	0 => t('TTDefault'),
	1 => t('None'),
	),
	 '#default_value' => theme_get_setting('style_footerbelowcolumn1'),
 );
$form[' ttr_settings']['tabs']['style_options']['style']['style_footerbelowcolumn2'] = array(
	'#type' => 'select',
	'#title' => t('Footerbelowcolumn2'),
	'#options' => array(
	0 => t('TTDefault'),
	1 => t('None'),
	),
	 '#default_value' => theme_get_setting('style_footerbelowcolumn2'),
 );
$form[' ttr_settings']['tabs']['style_options']['style']['style_footerbelowcolumn3'] = array(
	'#type' => 'select',
	'#title' => t('Footerbelowcolumn3'),
	'#options' => array(
	0 => t('TTDefault'),
	1 => t('None'),
	),
	 '#default_value' => theme_get_setting('style_footerbelowcolumn3'),
 );
$form[' ttr_settings']['tabs']['style_options']['style']['style_footerbelowcolumn4'] = array(
	'#type' => 'select',
	'#title' => t('Footerbelowcolumn4'),
	'#options' => array(
	0 => t('TTDefault'),
	1 => t('None'),
	),
	 '#default_value' => theme_get_setting('style_footerbelowcolumn4'),
 );
$form[' ttr_settings']['tabs']['style_options']['style']['style_leftfooterarea'] = array(
	'#type' => 'select',
	'#title' => t('Leftfooterarea'),
	'#options' => array(
	0 => t('TTDefault'),
	1 => t('None'),
	),
	 '#default_value' => theme_get_setting('style_leftfooterarea'),
 );
$form[' ttr_settings']['tabs']['style_options']['style']['style_centerfooterarea'] = array(
	'#type' => 'select',
	'#title' => t('Centerfooterarea'),
	'#options' => array(
	0 => t('TTDefault'),
	1 => t('None'),
	),
	 '#default_value' => theme_get_setting('style_centerfooterarea'),
 );
$form[' ttr_settings']['tabs']['style_options']['style']['style_rightfooterarea'] = array(
	'#type' => 'select',
	'#title' => t('Rightfooterarea'),
	'#options' => array(
	0 => t('TTDefault'),
	1 => t('None'),
	),
	 '#default_value' => theme_get_setting('style_rightfooterarea'),
 );
 }
